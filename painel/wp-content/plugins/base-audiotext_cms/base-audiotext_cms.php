<?php

/**
 * Plugin Name: Base Nome do Audiotext CMS
 * Description: Controle base do tema Audiotext CMS.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseAudiotext_cms () {

		// TIPOS DE CONTEÚDO
		conteudosAudiotext_cms();

		metaboxesAudiotext_cms();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosAudiotext_cms (){

		// TIPOS DE CONTEÚDO SERVIÇOS
		tipoServicos();

		// TIPOS DE CONTEÚDO REGRAS
		tipoRegras();

		// TIPOS DE CONTEÚDO FINALIDADE
		tipoFinalidade();

		// TIPOS DE CONTEÚDO IDIOMA
		tipoIdioma();

		// TIPOS DE CONTEÚDO ORÇAMENTO
		tipoOrcamento();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE SERVIÇOS
	function tipoServicos() {

		$rotulosServicos = array(
								'name'               => 'Servicos',
								'singular_name'      => 'servicos',
								'menu_name'          => 'Servicos',
								'name_admin_bar'     => 'Servicos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo servico',
								'new_item'           => 'Novo servico',
								'edit_item'          => 'Editar servicos',
								'view_item'          => 'Ver Servicos',
								'all_items'          => 'Todos os Servicos',
								'search_items'       => 'Buscar servico',
								'parent_item_colon'  => 'Dos Servicos',
								'not_found'          => 'Nenhum servico cadastrado.',
								'not_found_in_trash' => 'Nenhum servico na lixeira.'
							);

		$argsServicos	= array(
								'labels'             => $rotulosServicos,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-admin-tools',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'servicos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('servico', $argsServicos);

	}

	// CUSTOM POST TYPE REGRAS
	function tipoRegras() {

		$rotulosRegras = array(
								'name'               => 'Regras',
								'singular_name'      => 'regras',
								'menu_name'          => 'Regras',
								'name_admin_bar'     => 'Regras',
								'add_new'            => 'Adicionar nova',
								'add_new_item'       => 'Adicionar nova regra',
								'new_item'           => 'Nova regra',
								'edit_item'          => 'Editar regras',
								'view_item'          => 'Ver Regras',
								'all_items'          => 'Todas as Regras',
								'search_items'       => 'Buscar regra',
								'parent_item_colon'  => 'Das Regras',
								'not_found'          => 'Nenhuma regra cadastrado.',
								'not_found_in_trash' => 'Nenhuma regra na lixeira.'
							);

		$argsRegras	= array(
								'labels'             => $rotulosRegras,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-admin-network',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'regras' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('regra', $argsRegras);

	}

	// CUSTOM POST TYPE FINALIDADE
	function tipoFinalidade() {

		$rotulosFinalidade = array(
								'name'               => 'Finalidades',
								'singular_name'      => 'finalidades',
								'menu_name'          => 'Finalidades',
								'name_admin_bar'     => 'Finalidades',
								'add_new'            => 'Adicionar nova',
								'add_new_item'       => 'Adicionar nova finalidade',
								'new_item'           => 'Nova finalidade',
								'edit_item'          => 'Editar finalidades',
								'view_item'          => 'Ver Finalidades',
								'all_items'          => 'Todas as Finalidades',
								'search_items'       => 'Buscar finalidade',
								'parent_item_colon'  => 'Das Finalidades',
								'not_found'          => 'Nenhuma finalidade cadastrado.',
								'not_found_in_trash' => 'Nenhuma finalidade na lixeira.'
							);

		$argsFinalidade	= array(
								'labels'             => $rotulosFinalidade,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-editor-ul',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'finalidades'),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('finalidade', $argsFinalidade);

	}

	// CUSTOM POST TYPE IDIOMAS
	function tipoIdioma() {

		$rotulosIdioma = array(
								'name'               => 'Idioma',
								'singular_name'      => 'Idioma',
								'menu_name'          => 'Idiomas',
								'name_admin_bar'     => 'Idioma',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo idioma',
								'new_item'           => 'Novo idioma',
								'edit_item'          => 'Editar idioma',
								'view_item'          => 'Ver idioma',
								'all_items'          => 'Todos os idiomas',
								'search_items'       => 'Buscar idioma',
								'parent_item_colon'  => 'Dos idiomas',
								'not_found'          => 'Nenhum idioma cadastrado.',
								'not_found_in_trash' => 'Nenhum idioma na lixeira.'
							);

		$argsIdioma 	= array(
								'labels'             => $rotulosIdioma,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'idiomas' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('idioma', $argsIdioma);

	}

	// CUSTOM POST TYPE ORÇAMENTO
	function tipoOrcamento() {

		$rotulosOrcamento = array(
								'name'               => 'Orcamento',
								'singular_name'      => 'Orcamento',
								'menu_name'          => 'Orcamentos',
								'name_admin_bar'     => 'Orcamento',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo orçamento',
								'new_item'           => 'Novo orçamento',
								'edit_item'          => 'Editar orçamento',
								'view_item'          => 'Ver orçamento',
								'all_items'          => 'Todos os orçamentos',
								'search_items'       => 'Buscar orçamento',
								'parent_item_colon'  => 'Dos orçamentos',
								'not_found'          => 'Nenhum orçamento cadastrado.',
								'not_found_in_trash' => 'Nenhum orçamento na lixeira.'
							);

		$argsOrcamento 	= array(
								'labels'             => $rotulosOrcamento,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-clipboard',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'orcamentos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('orcamento', $argsOrcamento);

	}

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesAudiotext_cms(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Audiotext_cms_';

			// METABOX DE ORÇAMENTO
			$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxOrcamento',
				'title'			=> 'Detalhes do serviço',
				'pages' 		=> array( 'orcamento' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					  array(
						'name'  => 'teste: ',
						'id'    => "{$prefix}campoexemplo",
						'desc'  => '',
						'type'  => 'text'
					),
				),
			);

			// METABOX DE SERVIÇO
			$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxServicoMetrica',
				'title'			=> 'Detalhes do serviço',
				'pages' 		=> array( 'servico' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					  array(
		                'type'    => 'select_tree', // or checkbox_list
		                'id'      => 'servico_metrica',
		                'name'    => 'Métrica checkboxes',
		                'options' => array(
		                    array( 'value' => 'Minuto', 'label' => 'Minuto' ),
		                    array( 'value' => 'Palavra', 'label' => 'Palavra' ),
		                    array( 'value' => 'Pagina', 'label' => 'Pagina' ),
		                   
		                ),
		                'flatten' => false,
		            ),
				),
			);

			// METABOX DE SERVIÇO/PRECIFICAÇÃO FIXA
			$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxServiPrecificacaoFaixa',
				'title'			=> 'Detalhes da precificação fixa',
				'pages' 		=> array( 'servico' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Faixa/MIN: ',
						'id'    => "{$prefix}servicos_faixa_min",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Faixa/MAX: ',
						'id'    => "{$prefix}servicos_faixa_max",
						'desc'  => '',
						'type'  => 'text'
					),
				),
			);

			// METABOX DE SERVIÇO/PRECIFICAÇÃO POR MINUTOS
			$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxServiPrecificacaoMinuto',
				'title'			=> 'Detalhes da precificação por minuto',
				'pages' 		=> array( 'servico' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Minuto/MIN: ',
						'id'    => "{$prefix}servicos_minuto_min",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Minuto/MAX: ',
						'id'    => "{$prefix}servicos_minuto_max",
						'desc'  => '',
						'type'  => 'text'
					),
				),
			);

			// METABOX DE SERVIÇO/PRECIFICAÇÃO CUSTUMIZADA
			$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxServicoCustumizada',
				'title'			=> 'Detalhes da precificação custumizada',
				'pages' 		=> array( 'servico' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Minuto/MIN: ',
						'id'    => "{$prefix}servico_custumizada_min",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => '* infinito: ',
						'id'    => "{$prefix}servico_custumizada_infitino",
						'desc'  => 'Não precisa preencher',
						'type'  => 'text'
					),
				),
			);

			// METABOX DE REGRA
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxRegra',
				'title'			=> 'Detalhes da regra',
				'pages' 		=> array( 'regra' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  	 => 'Serviço: ',
						'id'    	 => "{$prefix}regra_servico",
						'desc'  	 => '',
						'type'  	 => 'post',
						'post_type'  => 'servico',
						'field_type' => 'select'
					),
					array(
						'name'  	 => 'Utilizar preço fixo: ',
						'id'    	 => "{$prefix}sregra_preco_fixo",
						'desc'  	 => '',
						'type'  	 => 'checkbox',
					),
					array(
						'name'  	 => 'Finalidade juridica: ',
						'id'    	 => "{$prefix}regra_finalidade_juridica",
						'desc'  	 => '',
						'type'  	 => 'checkbox',
					),
					array(
						'name'  	 => 'Precificação de idioma ',
						'id'    	 => "{$prefix}regra_precificacao_idioma",
						'desc'  	 => '',
						'type'  	 => 'text',
					),
					array(
						'name'  	 => 'Taxa de parcelamento',
						'id'    	 => "{$prefix}regra_taxa_parcelamento",
						'desc'  	 => '',
						'type'  	 => 'text',
					),
					array(
						'name'  	 => 'Parcelamento máximo: ',
						'id'    	 => "{$prefix}regra_parcelamento_max",
						'desc'  	 => '',
						'type'  	 => 'number',
					),
					array(
						'name'  	 => 'Instant ',
						'id'    	 => "{$prefix}regra_instant",
						'desc'  	 => '',
						'type'  	 => 'number',
					),
					array(
						'name'  	 => 'Express ',
						'id'    	 => "{$prefix}regra_express",
						'desc'  	 => '',
						'type'  	 => 'number',
					),
					array(
						'name'  	 => 'Fast ',
						'id'    	 => "{$prefix}regra_fast",
						'desc'  	 => '',
						'type'  	 => 'number',
					),
					array(
						'name'  	 => 'Flex ',
						'id'    	 => "{$prefix}regra_flex",
						'desc'  	 => '',
						'type'  	 => 'number',
					),
				),
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesAudiotext_cms(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerAudiotext_cms(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseAudiotext_cms');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseAudiotext_cms();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );