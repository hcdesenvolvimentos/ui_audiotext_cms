<?php
/**
 * Template Name: Inicial
 * Description:
 *
 * @package Audiotext
 */
// CADASTRO DE ORÇAMENTO
    if($_SERVER['REQUEST_METHOD'] == 'POST'){

        if(isset($_POST['orcamentoCadastro'])){
          
            $orcamentoNome             = $_POST['orcamentoNome'];     
            $orcamentoMetaboxTeste     = $_POST['orcamentoMetaboxTeste'];     

            $cadastroOrcamento = array(
                'post_title'    => $orcamentoNome,
                'post_content'  => '',
                'post_status'   => 'publish',
                'post_type'     => 'orcamento'
            );
            
            $pid = wp_insert_post($cadastroOrcamento);

            // METABOXES
            add_post_meta($pid, 'Audiotext_cms_campoexemplo',$orcamentoMetaboxTeste,true);
        }

            if($pid > 0){ $cadastroRealizado = true; }

    }

get_header();

?>
 <form id="formulario-cadastro-po" method="post" enctype="multipart/form-data">
    <input type="text" id="orcamentoNome" name="orcamentoNome">
    <input type="text" id="orcamentoMetaboxTeste" name="orcamentoMetaboxTeste">
    <input type="hidden" name="orcamentoCadastro" value="1">
    <input type="submit" class="btn" value="Cadastrar">
</form>
<?php get_footer(); ?>