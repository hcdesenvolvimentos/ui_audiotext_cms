const { hooks } = require('@adonisjs/ignitor')

hooks.after.providersBooted(() => {
  const View = use('View');
  View.global('translateMetric', (metric, amount) => {
    switch (metric) {
      case 'minute':
        return amount === 1 ? `${amount} minuto` : `${amount} minutos`;
      case 'word':
        return amount === 1 ? `${amount} palavra` : `${amount} palavras`;
      case 'page':
        return amount === 1 ? `${amount} página` : `${amount} páginas`;
      default:
        return amount === 1 ? `${amount} minuto` : `${amount} minutos`;
    }
  });

  View.global('maxInstallment', value => {
    if (value > 1) {
      return `em até ${value} vezes`;
    }
    return `em até ${value} vez`;
  })

  View.global('getPlanColor', plan => {
    const map = new Map();
    map.set('instant', '#F15A22');
    map.set('express', '#EFC100');
    map.set('fast', '#76B301');
    map.set('flex', '#00B0D8');
    return map.get(plan);
  })

  View.global('transliterateService', serviceCode => {
    switch (serviceCode) {
      case 'transcricao':
        return 'transcritos';
      case 'legendagem':
        return 'lengendados';
      default:
        return 'traduzidos';
    }
  })

  View.global('formatMoney', num => {
    try {
      const res = num.split(",");
      if(!res[1]) {
        return `${num},00`;
      } else if (res[1].length === 1) {
        return `${num}0`;
      }
    } catch (error) {
      console.error(error);
    }
    return num;
  })
});
