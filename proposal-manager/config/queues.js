'use strict'

const Env = use('Env');

const username = Env.get('RABBITMQ_DEFAULT_USER', 'user');
const password = Env.get('RABBITMQ_DEFAULT_PASS', 'password');
const host = Env.get('RABBITMQ_HOST', '127.0.0.1');
const port = Env.get('RABBITMQ_PORT', '5672');
const vhost = Env.get('RABBITMQ_DEFAULT_VHOST', '');

module.exports = {
  driver: 'rabbitmq',
  rabbitmq: {
    url: `amqp://${username}:${password}@${host}:${port}/${vhost}`,
    consumers: []
  }
};
