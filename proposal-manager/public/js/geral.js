$(function(){

	$("#gerarPropostas").click(function(e) {
		// Generate proposals
		var form = $(this).parent('form');
		var formSerialized = serializeToJSON(form.serializeArray());
    // Disable button
    $(this).css("background", "#6876ab");
    $(this).text('Gerando Propostas...');
		generateProposals(formSerialized);
		e.preventDefault();
	});

	$("#proximaEtapa").click(function(e) {
		// Save form
		saveForm($(this).parent('form'));
		$("#primeiraEtapa").fadeOut();
		setTimeout(function(){
			$("#segundaEtapa").slideDown();
		}, 500);
		e.preventDefault();
	});

	$("#voltar").click(function(e) {
		// Save form
		saveForm($(this).parent('form'));
		setTimeout(function(){
			$("#primeiraEtapa").slideDown();
		}, 500);
		$("#segundaEtapa").fadeOut();
		e.preventDefault();
	});

	function patchBudgetForm(budget) {
		$.ajax({
			url: $('#apiEndpoints').data('save-form'),
			method: 'PATCH',
			timeout: 10000,
			dataType: 'json',
			contentType: 'application/json',
			headers: {'X-XSRF-TOKEN': budget._csrf || null},
			data: JSON.stringify(budget),
			success: function (data) {
				console.info(data);
			},
			error: function(message)
			{
				console.error(message.responseText);
			}
		});
	}

	function generateProposals(budget) {
		$.ajax({
			url: $('#apiEndpoints').data('generate-proposals'),
			method: 'POST',
			timeout: 10000,
			dataType: 'json',
      async: false,
			contentType: 'application/json',
			headers: {'X-XSRF-TOKEN': budget._csrf || null},
			data: JSON.stringify(budget),
			success: function (response) {
			  // Clear all error span labels
        $('.formularioOrcamento .validacao').removeClass('compoInvalido');
        // Check by response form errors
        if (response.hasErrors) {
          // TODO - Display general message to notify errors
          var errors = response.errors;
          for (var i = 0; i < errors.length; i++) {
            var key = Object.keys(errors[i])[0];
            var message = errors[i][key].message;
            var field = $("#validation-"+key);
            field.addClass('compoInvalido');
            field.text(message);
          }
        } else {
          // if not has errors, redirect to proposals page
          window.location = $('#apiEndpoints').data('get-proposals');
        }
			},
			error: function(message)
			{
				console.error(message);
			},
      complete: function (message) {
			  var btn = $('#gerarPropostas');
        btn.css("background", "#2f3b6b");
        btn.text('Solicitar Orçamento');
      }
		});
	}

	function saveForm(form) {
		// Serialize form data
		var serialized = serializeToJSON(form.serializeArray());
		// Patch messages.json.json data
		return patchBudgetForm(serialized);
	}

	function serializeToJSON(fields) {
		var budget = {};
		$.each(fields, function(index) {
			budget[fields[index].name] = fields[index].value;
		});
		return budget;
	}
});
