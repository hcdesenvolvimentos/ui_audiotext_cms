'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class BudgetSession {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ session, request }, next) {
    let {sessionCode} = request.all();
    if (!sessionCode || typeof sessionCode === 'undefined') {
      sessionCode = session._sessionId;
    }
    Object.assign(request.params, {sessionCode, csrfToken: request.csrfToken});
    await next()
  }
}

module.exports = BudgetSession;
