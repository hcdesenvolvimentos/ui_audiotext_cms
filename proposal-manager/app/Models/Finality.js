'use strict'
const Parser = require('@montacasa/parser');
/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Schema = use('App/Models/Abstract/Schema');

class Finality extends Schema {
    static schema() {
        return {
            attributes: {
                code: {type: 'string', required: true},
                name: {type: 'string', required: true},
            },
        };
    }

    /**
     * Create new finality
     * @param {any} params - parameters object
     */
    static async createFinality(params) {
        const { name } = params;

        const rule = {lower: true, remove: null, replacement: '-'};
        if (!name) {
            throw new Error('Missing finality name!');
        }
        const code = Parser.slugify(name, rule);
        return await this.findOrCreate({code}, {
            code: code,
            name: name, 
        });
    }
}

module.exports = Finality
