'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Schema = use('App/Models/Abstract/Schema');
const Service = use('App/Models/Service');

class PriceRule extends Schema {
    static schema() {
        return {
            attributes: {
                serviceCode: {type: 'string', required: true},
                fixedPrice: {type: 'boolean', required: true},
                maxInstallment: {type: 'number', default: 1},
                minRange: {type: 'number', default: 0},
                maxRange: {type: 'number', default: 0},
                plans: {type: 'object', default: []},
                legalProposeTax: {type: 'number', default: 1},
                languageTax: {type: 'number', default: 1},
                installmentTax: {type: 'number', default: 1},
                enabled: {type: 'boolean', default: true},
            },
        };
    }

  static get visible () {
    return [
      '_id',
      'serviceCode',
      'fixedPrice',
      'maxInstallment',
      'minRange',
      'maxRange',
      'plans',
      'legalProposeTax',
      'languageTax',
      'installmentTax',
      'enabled',
    ];
  }

    service () {
      return this.hasOne('App/Models/Service', 'serviceCode', 'code');
    }

     /**
     * Create new price rule
     * @param {any} params - parameters object
     */
    static async createRule(params) {
        const { 
            serviceCode, 
            range, 
            fixedPrice,
            maxInstallment,
            minRange,
            maxRange,
            plans,
            legalProposeTax,
            languageTax,
            installmentTax,
            enabled,
        } = params;

        const service = await Service.query().where({code: serviceCode}).first();
        if (!service) {
            throw new Error('Service not found!');
        }

        return await this.create({
            serviceCode,
            range,
            fixedPrice,
            minRange,
            maxRange,
            maxInstallment,
            plans,
            legalProposeTax,
            languageTax,
            installmentTax,
            enabled,
        });
    }

    /**
     * Find price rule by service and range interval
     * @param {Budget} budget - budget object 
     */
    static async findPriceRule(budget) {
        try {
            return await this.query()
            .where({serviceCode: budget.service.code})
            .where({
                $and: [
                    {minRange: {$lte: budget.amount}},
                    {maxRange: {$gte: budget.amount}},
                    {enabled: {$eq: true}},
                ],
                })
            .first();
        } catch (error) {
            console.error(error.message);
        }
        return null;
    }

  /**
   * Update price rule by id
   * @param params
   * @returns {Promise<*>}
   */
  static async updatePriceRule(params) {
    let {installmentTax, languageTax, legalProposeTax, maxInstallment, plans, ruleId} = params;
    if (!ruleId) {
      throw new Error('Missing rule id param!');
    }
    const rule = await this.query().where({_id: ruleId}).first();
    if (!rule) {
      throw new Error('Rule not found!');
    }

    rule.installmentTax = installmentTax || rule.installmentTax;
    rule.languageTax = languageTax || rule.languageTax;
    rule.legalProposeTax = legalProposeTax || rule.legalProposeTax;
    rule.maxInstallment = maxInstallment || rule.maxInstallment;

    plans = Array.isArray(plans) ? plans.map(p => {
      return {
        name: p.name,
        value: p.value && typeof p.value === 'number' ? p.value : null,
        deadline: p.deadline && typeof p.deadline === 'number' ? p.deadline : null,
      };
    }) : null;

    if (plans) {
      rule.plans = plans;
    }

    await rule.save();
    return rule;
  }
}

module.exports = PriceRule;
