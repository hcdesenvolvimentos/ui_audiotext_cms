'use strict'
const Parser = require('@montacasa/parser');
/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Schema = use('App/Models/Abstract/Schema');

class Service extends Schema {
    static schema() {
        return {
            attributes: {
                code: {type: 'string', required: true},
                name: {type: 'string', required: true},
                metric: {type: 'string', required: true}, // Possible metrics: minutes/words/pages
                fixedRange: {type: 'string', required: true},
                metricRange: {type: 'string', required: true},
                customRange: {type: 'string'},
                enabled: {type: 'boolean', default: true},
            },
        };
    }

    // Service metrics
    static get MINUTE() {
        return 'minute';
    }

    static get WORD() {
        return 'word';
    }

    static get PAGE() {
        return 'page';
    }

    /**
     * Create new service
     * @param {any} params - parameters object
     */
    static async createService(params) {
        const {
            name, 
            metric, 
            fixedRange, 
            metricRange, 
            customRange,
            enabled,
        } = params;

        const rule = {lower: true, remove: null, replacement: '-'};
        if (!name) {
            throw new Error('Missing service name!');
        }
        const code = Parser.slugify(name, rule);
        return await this.findOrCreate({code}, {
            code: code,
            name: name, 
            metric: metric, 
            fixedRange: fixedRange, 
            metricRange: metricRange,
            customRange: customRange,
            enabled: enabled,
        });
    }
}

module.exports = Service
