'use strict'

const Antl = use('Antl');
const View = use('View');
class ProposalTextFormatter {
  static async format(budget) {
    let text = '';
    if (budget.custom) {
      text = Antl.formatMessage('proposal.text.for.custom');
    } else if (!budget.custom && budget.proposals && budget.proposals.length > 0) {
      // Format money
      for(let proposal of budget.proposals) {
        proposal.plan = proposal.plan.toUpperCase();
        proposal.cashPirce = Antl.formatNumber(proposal.cashPirce.toFixed(2), {currency: 'BRL', style: 'currency', maximumFractionDigits: 2});
        proposal.installmentPrice = Antl.formatNumber(proposal.installmentPrice.toFixed(2), {currency: 'BRL', style: 'currency', maximumFractionDigits: 2});
        proposal.installmentValue = Antl.formatNumber(proposal.installmentValue.toFixed(2), {currency: 'BRL', style: 'currency', maximumFractionDigits: 2});
      }
      budget.service.metric = View.engine._globals.translateMetric(budget.service.metric, budget.amount);
      // Format text
      budget.maxInstallment = View.engine._globals.maxInstallment(budget.maxInstallment);
      text = Antl.formatMessage('proposal.text.for.non.custom', {
        username: budget.username,
        serviceName: budget.service.name.toLocaleLowerCase(),
        serviceMetric: budget.service.metric,
      });
    } else {
      text = Antl.formatMessage('proposal.text.for.not.found');
    }
    Object.assign(budget, {text: text});
    return budget;
  }
}

module.exports = ProposalTextFormatter;
