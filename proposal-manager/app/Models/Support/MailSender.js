'use strict'

const Schema = use('App/Models/Abstract/Schema');
const Mail = use('Mail');
const Antl = use('Antl');
const Env = use('Env');
class MailSender extends Schema {
  /**
   * Send budget proposals email
   * @param budget {Budget} - budget instance
   * @returns {Promise<*>}
   */
    static async sendProposals(budget) {
      // Check by custom proposal
      if (Array.isArray(budget.proposals)) {
        const from = Env.get('EMAIL_FROM', 'sender.email@example.com.br,Sender Name | Audiotext').split(',');
        const subject = Env.get('EMAIL_SUBJECT', `Orçamento ${budget.service.name} | Audiotext`);
        const to = [budget.email].concat(Env.get('EMAIL_CC', []).split(',').filter(e =>  e.trim() !== ''));
        return await Mail.send('budget.email', {budget, antl: Antl}, message => {
          message
            .to(to)
            .from(from[0], from[1])
            .subject(subject)
        });
      }
      console.log(`Budget not has proposals! Customer: ${budget.username} (${budget.email}) }`);
    }
}

module.exports = MailSender;
