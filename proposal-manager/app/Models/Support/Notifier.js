'use strict'

const Env = use('Env');
const slackConfs = Env.get('SLACK_WEBHOOK_CHANNEL', '|').split('|');
const slackDebbugConfs = Env.get('SLACK_WEBHOOK_CHANNEL_DEBUGGER', '|').split('|');
const slackNotify = require('slack-notify')(`https://hooks.slack.com/services/${slackConfs[0]}`);
const slackNotifyDebugger = require('slack-notify')(`https://hooks.slack.com/services/${slackDebbugConfs[0]}`);
const Publisher = use('RabbitMQ/Queue/Producer');

class Notifier {
  constructor() {
    this.slackNotify = slackNotify.extend({
      channel: `#${slackConfs[1]}`,
      icon_emoji: ':spider:',
      username: 'Audiotext CMS',
    });
    this.slackNotifyDebugger = slackNotifyDebugger.extend({
      channel: `#${slackDebbugConfs[1]}`,
      icon_emoji: ':spider:',
      username: 'Audiotext CMS',
    });
  }

  /**
   * Prepare message, save it and use alert method to notify
   * @param title {string} - title message
   * @param data {any} - object message
   * @returns {Promise<void>}
   */
  async alert(title, data) {
    try {
      if (slackConfs[0] && slackConfs[1]) {
        return await this.notify(title, data);
      }
      console.log('Notifier -> No Slack URL configured.');
    } catch (error) {
      console.error(error.message);
    }
  }

  /**
   * Send alert notification to slack channel
   * @param title {string} - title notification
   * @param fields {any} - notification data fields
   * @returns {Promise<any>}
   */
  notify(title, fields) {
    return new Promise((resolve, reject) => {
      try {
        this.slackNotifyDebugger({
          text: title,
          fields: fields,
        });
        resolve(
          this.slackNotify({
            text: title,
            fields: fields,
          }),
        );
      } catch (error) {
        reject(error);
      }
    });
  }

  /**
   * Publish budget message to queue
   * @param budget
   */
  async publish(budget) {
    try {
      const options = {deliveryMode: true, contentType: 'application/json'};
      const message = JSON.stringify(budget);
      const queue = 'budget';
      await Publisher.publish(queue, message, options);
    } catch (e) {
      console.error(e);
      // TODO - Save error logs in DB
    }
  }
}

module.exports = Notifier;
