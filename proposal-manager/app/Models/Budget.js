'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Antl = use('Antl');
const Schema = use('App/Models/Abstract/Schema');
const Service = use('App/Models/Service');
const Finality = use('App/Models/Finality');
const Proposal = use('App/Models/Proposal');
const Language = use('App/Models/Language');
const MailSender = use('App/Models/Support/MailSender');
const ProposalManager = use('App/Models/ProposalManager');
const ProposalTextFormatter = use('App/Models/Support/ProposalTextFormatter');
const Notifier = use ('App/Models/Support/Notifier');

class Budget extends Schema {
    static schema() {
        return {
            attributes: {
                sessionCode: {type: 'string', required: true},
                username: {type: 'string'},
                company: {type: 'string'},
                email: {type: 'string'},
                phone: {type: 'string'},
                isWhatsApp: {type: 'boolean', default: true},
                serviceCode: {type: 'string'}, // reference service code
                amount: {type: 'number', default: 0}, // amount of metric: 2min/2words/2pages
                participantsAmount: {type: 'number', default: null}, // participantsAmount amount (only to audio transcriptions)
                finalityCode: {type: 'string'}, // reference finality code
                languageCode: {type: 'string'}, // reference language code
                howDidMeetUs: {type: 'string'}, // how did you meet us?
                status: {type: 'string', default: 'pending'}, // budget status, it always start with pending status
                observation: {type: 'string'}, // some additional observation
                maxInstallment: {type: 'number', default: 1}, // max installment, it can be defined by rule
                discount: {type: 'number', default: 0},
                proposalChosen: {type: 'object'}, // proposal chosen
                custom: {type: 'boolean', default: false},
                utmSource: {type: 'string'},
                utmMedium: {type: 'string'},
                utmCampaign: {type: 'string'},
                utmTerm: {type: 'string'},
                utmContent: {type: 'string'},
                userAgent: {type: 'string'},
                ip: {type: 'string'},
            },
        };
    }

    proposals () {
        return this.hasMany('App/Models/Proposal', '_id', 'budgetId');
    }

    proposal() {
      return this.belongsTo('App/Models/Proposal', 'proposalChosen', '_id');
    }

    service () {
        return this.hasOne('App/Models/Service', 'serviceCode', 'code');
    }

    finality () {
        return this.hasOne('App/Models/Finality', 'finalityCode', 'code');
    }

    language () {
        return this.hasOne('App/Models/Language', 'languageCode', 'code');
    }

    // Budget status
    static get PENDING() {
        return 'PENDING';
    }

    static get APPROVED() {
        return 'APPROVED';
    }

    static get SENT() {
      return 'SENT';
    }

    static get REJECTED() {
        return 'REJECTED';
    }

    static getMeetingChannels() {
        return [
            'Já sou cliente',
            'Indicação',
            'Google',
            'Outro',
        ];
    }

    /**
     * Get budget by session code or create a new budget if not found
     * @param {String} sessionCode - session code
     * @param {object} params - additional params
     */
    static async getBySession(sessionCode, params = {}) {
        if (!sessionCode) {
            throw new Error('Missing session code param!');
        }
        const budget = await this.findOrCreate({sessionCode/*, status: this.PENDING*/}, {
            sessionCode: sessionCode,
            username: null,
            company: null,
            email: null,
            phone: null,
            isWhatsApp: true,
            serviceCode: null,
            amount: 0,
            participantsAmount: null,
            finalityCode: null,
            languageCode: null,
            status: this.PENDING,
            howDidMeetUs: null,
            observation: null,
            maxInstallment: null,
            discount: 0,
            proposalChosen: null,
            custom: false,
            utmSource: null,
            utmMedium: null,
            utmCampaign: null,
            utmTerm: null,
            utmContent: null,
            userAgent: null,
            ip: null,
        });

        if (budget) {
          budget.utmSource = params.utmSource || budget.utmSource;
          budget.utmMedium = params.utmMedium || budget.utmMedium;
          budget.utmCampaign = params.utmCampaign || budget.utmCampaign;
          budget.utmTerm = params.utmTerm || budget.utmTerm;
          budget.utmContent = params.utmContent || budget.utmContent;
          budget.userAgent = params.userAgent || budget.userAgent;
          budget.ip = params.ip || budget.ip;
          await budget.save();
        }
        return budget;
    }

  /**
   * Load budget relations
   * @returns {Promise<void>}
   */
  async loadRelations() {
      await this.load(['service']);
      await this.load(['finality']);
      await this.load(['language']);
    }

    /**
     * Update budget
     * @param {any} params - object parameters
     */
    async updateBudget (params) {
        const {
            username,
            company,
            email,
            phone,
            isWhatsApp,
            serviceCode,
            amount,
            participantsAmount,
            finalityCode,
            languageCode,
            howDidMeetUs,
            observation,
            utmSource,
            utmMedium,
            utmCampaign,
            utmTerm,
            utmContent,
            userAgent,
            ip,
        } = params;

        // Check by service and associate it
        const service = await Service.query().where({code: serviceCode}).first();
        if (service) {
            this.serviceCode = service.code;
        }
        // Check by finality and associate it
        const finality = await Finality.query().where({code: finalityCode}).first();
        if (finality) {
            this.finalityCode= finality.code;
        }

        // Check by language and associate it
        const language = await Language.query().where({code: languageCode}).first();
        if (language) {
            this.languageCode = language.code;
        }
        this.username = username || this.username;
        this.company = company || this.company;
        this.email = email || this.email;
        this.phone = phone || this.phone;
        this.isWhatsApp = typeof isWhatsApp !== 'undefined' ? isWhatsApp : this.isWhatsApp;
        this.amount = amount || this.amount;
        this.participantsAmount = participantsAmount || this.participantsAmount;
        this.howDidMeetUs = howDidMeetUs || this.howDidMeetUs;
        this.observation = observation || this.observation;
        this.utmSource = utmSource || this.utmSource;
        this.utmMedium = utmMedium || this.utmMedium;
        this.utmCampaign = utmCampaign || this.utmCampaign;
        this.utmTerm = utmTerm || this.utmTerm;
        this.utmContent = utmContent || this.utmContent;
        this.userAgent = userAgent || this.userAgent;
        this.ip = ip || this.ip;

        await this.save();
        return this;
    }

    /**
     * Validate budget required fields
     */
    static async validateBudget(instance) {
      return new Promise((resolve, reject) => {
        try {
          const v = {
            hasErrors: false,
            errors: [],
          };
          const requiredFields = [
            'username',
            'email',
            'phone',
            'service',
            'amount',
            'finality',
            'language',
            'howDidMeetUs',
            'participantsAmount',
          ];
          for (const name of requiredFields) {
            if (!instance[name]) {
              const obj = {};
              obj[name] = {message: Antl.formatMessage(`budget.form.field.${name}`)};
              v.errors.push(obj);
              v.hasErrors = true;
            }
          }
          resolve(v);
        } catch (error) {
          reject (error);
        }
      });
    }

    /**
     * Use proposal manager to generate budget proposals
     */
    async generateProposals() {
        const pm = new ProposalManager();
        await pm.generateProposals(this);
        await this.load(['proposals']);
        return this;
    }


  async getPrettyFormat() {
    return ProposalTextFormatter.format(this.toJSON());
  }

  /**
   * Send proposals generated to customer with copy to seller
   * @returns {Promise<Uint8Array | boolean[] | Int32Array | Uint16Array | Uint32Array | Float64Array | any>}
   */
  async sendProposals() {
    const notifier = new Notifier();
    const budget = this.toJSON();
    let error = null;
    // Remove some fields before send it
    // delete budget.utmCampaign;
    delete budget.userAgent;
    delete budget.discount;
    try {
      console.log('Sending proposal by email...');
      await MailSender.sendProposals(this.toJSON());
      this.status = Budget.SENT;
      await this.save();
    } catch (error) {
      error = `Ocorreu um erro no envio das propostas: ${error.message}`;
      console.error(error);
    }

    try {
      await this.save();
      notifier.publish(budget);
    } catch (error) {
      console.error(error);
    }

    return await notifier.alert('Evento: Gerar Proposta', error ? Object.assign(budget, {error}) : budget);
  }

  /**
   * Set budget chosen proposal
   * @param plan {string} - plan name
   * @returns {Promise<void>}
   */
  async setChosenProposal(plan) {
    // Find proposal
    const proposal = await Proposal.query().where({budgetId: this._id, plan}).first();
    // const proposal = this.toJSON().proposals.find(p => p.plan === plan);
    if (proposal) {
      // Associate chosen proposal
      await this.proposal().associate(proposal);
    }

  }

}

module.exports = Budget;
