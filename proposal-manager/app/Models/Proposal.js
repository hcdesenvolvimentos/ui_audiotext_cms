'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Schema = use('App/Models/Abstract/Schema');

class Proposal extends Schema {
    static schema() {
        return {
            attributes: {
                budgetId: {type: 'object', required: true},
                plan: {type: 'string', required: true},
                cashPirce: {type: 'number'},
                installmentPrice: {type: 'number'},
                installmentValue: {type: 'number'},
                deadline: {type: 'number', default: 1},
            },
        };
    }

    static get objectIdFields() {
        return ['_id', 'budgetId'];
    }
}

module.exports = Proposal;
