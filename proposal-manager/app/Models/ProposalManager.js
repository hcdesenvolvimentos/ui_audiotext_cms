'use strict'

const PriceRule = use('App/Models/PriceRule');
const Proposal = use('App/Models/Proposal');
class ProposalManager {
  async generateProposals(budget) {
    let instanceBudget = budget.toJSON();
    // Check if budget is into custom range
    const intoCustomRange = instanceBudget.amount && instanceBudget.amount >= instanceBudget.service.customRange.split('-', 1);

    // If budget is into custom range or if budget language not is pt-BR or en-US
    budget.custom = intoCustomRange || (instanceBudget.language.code !== 'pt-BR' && instanceBudget.language.code !== 'en-US');
    await budget.save();

    if (budget.custom) {
      return this;
    }

    const rule = await PriceRule.findPriceRule(instanceBudget);
    if (!rule) {
      console.error('No rules found for this budget input!');
      return this;
    }
    // Set budget max installment
    budget.maxInstallment = rule.maxInstallment;
    await budget.save();

    // Truncate any proposal generated to this budget before generated new proposals
    await Proposal.query().where({budgetId: budget._id}).delete();
    for (const plan of rule.plans) {
      if (plan.value) {
        // Initialize new proposal instance
        let proposal = new Proposal();
        proposal.budgetId = budget._id;
        proposal.plan = plan.name;
        proposal.deadline = plan.deadline;

        let total = 0;
        // Check and apply fixedPrice rule
        if (rule.fixedPrice) {
          total = plan.value;
          if (instanceBudget.finality.code === 'juridica') {
            total = total * rule.legalProposeTax;
          }
        } else {
          // Check and apply legal propose rule
          if (instanceBudget.finality.code === 'juridica') {
            total = (plan.value * rule.legalProposeTax) * instanceBudget.amount;
          } else {
            total = plan.value * instanceBudget.amount;
          }
        }

        if (instanceBudget.language.code !== 'pt-BR') {
          total = total * rule.languageTax;
        }

        // TODO - Developt installment calc
        proposal.cashPirce = await this.calculate(total.toFixed(2));
        // define total installment price
        proposal.installmentPrice = await this.calculate(Math.round(proposal.cashPirce * rule.installmentTax).toFixed(2));
        // define price per installment
        proposal.installmentValue = await this.calculate(Math.round(proposal.installmentPrice / rule.maxInstallment).toFixed(2));

        await proposal.save();
        console.log(`Cash: ${plan.name} -> ${Math.round(total)}`);
        console.log(`Total Installment: ${plan.name} -> ${proposal.installmentPrice}`);
        console.log(`Installment Value: ${plan.name} -> ${proposal.installmentValue}`);
      }
    }
    return this;
  }

  async calculate(expression) {
    return new Promise((resolve, reject) => {
      return resolve(expression);
    });
  }
}

module.exports = ProposalManager;
