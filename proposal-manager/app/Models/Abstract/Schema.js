'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model');
const Parser = require('@montacasa/parser');

class Schema extends Model {
    static get hidden() {
        return ['_id'];
    }
    static schema() {
        return {
          attributes: {},
        };
      }

      /**
       * Override findOrCreate method
       * @param keys {object} - object attributes data
       * @param data {object} - object attributes data
       * @returns {Promise<*>}
       */
      static async findOrCreate(keys, data) {
        let model = null;
        let ModelClass = this;
    
        try {
          model = await ModelClass.query()
            .where(keys)
            .first();
          if (model) {
            return model;
          }

          if (!data) {
            throw new Error('Missing attributes data!');
          }
          let obj = Object.assign(data, keys);
          await ModelClass.create(obj);
          return await ModelClass.query()
            .where(keys)
            .first();
        } catch (err) {
          console.error(`${ModelClass.name} | Method: BaseModel.findOrCreate`);
          throw err;
        }
      }
    
      /**
       * Overriding save method to apply validations before save data
       * @returns {*}
       */
      async save () {
        if (!this.$persisted) {
          this.insertValidation(this.constructor.schema());
        } else {
          this.updateValidation(this.constructor.schema());
        }
        return await super.save();
      }
    
      /**
       * Validation to new instances tha will be saved
       * @param schema {object} - object schema from current class
       */
      insertValidation(schema) {
        this.validatePresenceOfAttrs(schema);
        this.filterAttributes(schema);
        this.fillAttrsWithDefaultValues(schema);
        this.validateRequiredAttrs(schema);
        this.validateAttrValueType(schema);
      }
    
      /**
       * Validation to instances tha will be updated
       * @param schema {object} - object schema from current class
       */
      updateValidation(schema) {
        this.filterAttributes(schema);
        this.validateAttrValueType(schema);
      }
    
      /**
       * Validate the presence of attributes
       * @param schema {object} - attributes schema
       * @returns {boolean}
       */
      validatePresenceOfAttrs(schema) {
        const schemaAttributesKeys = Object.keys(schema.attributes);
        const errorsArr = [];
        const dataKeys = Object.keys(this.$attributes);
        const difference = schemaAttributesKeys.filter(attr => !dataKeys.includes(attr));
        if (difference.length > 0) {
          for (let attr of difference) {
            errorsArr.push(attr);
          }
          let error = `${difference.length} missing attribute(s): ${errorsArr.toString()}. You must enter this field(s)!`;
          throw new Error(error);
        }
        return true;
      }
    
      /**
       * Filter attributes
       * @param schema {object} - attributes schema
       * @returns {BaseModel}
       */
      filterAttributes(schema) {
        let attributes = Object.keys(schema.attributes);
    
        let attrs = new Map();
        new Set(attributes).forEach(attr => {
          attrs.set(attr, attr);
        });
    
        new Set(Object.keys(this.$attributes)).forEach(attr => {
          if (!attrs.has(attr) && attr != 'created_at' && attr != 'updated_at' && attr != '_id') {
            delete this.$attributes[attr];
          }
        });
        return this;
      }
    
      /**
       * Validate required attributes
       * @param schema {object} - attributes schema
       * @returns {boolean}
       */
      validateRequiredAttrs(schema) {
        let attributes = schema.attributes;
        let schemaAttributesKeys = Object.keys(attributes);
        let errorsArr = [];
    
        for (const attr of new Set(schemaAttributesKeys)) {
          let value = this.$attributes[attr];
          this.$attributes[attr] =
            (value === null || typeof value === 'undefined')
              ? null
              : this.$attributes[attr];
          if (
            typeof attributes[attr].required === 'boolean' &&
            attributes[attr].required === true &&
            (
              this.$attributes[attr] === null ||
              typeof value === 'string' && value.trim() === ''
            )
          ) {
            errorsArr.push(attr);
          }
        }
    
        if (errorsArr.length > 0) {
          let error = `Required attributes: ${errorsArr.toString()}`;
          throw new Error(error);
        }
        return true;
      }
    
      /**
       * Fill attributes with default values
       * @param schema {object} - attributes schema
       * @returns {BaseModel}
       */
      fillAttrsWithDefaultValues(schema) {
        let attributes = schema.attributes;
        let schemaAttributesKeys = Object.keys(attributes);
        for (const attr of new Set(schemaAttributesKeys)) {
          let value = this.$attributes[attr];
          if (typeof value !== 'undefined' && value !== null) {
            this.$attributes[attr] = value;
          } else if ((typeof value === 'undefined' || value === null) && 'default' in attributes[attr]) {
            this.$attributes[attr] = attributes[attr].default;
          } else {
            this.$attributes[attr] = null;
          }
        }
        return this;
      }
    
      /**
       * Validate attribute value type
       * @param schema {object} - attributes schema
       * @returns {boolean}
       */
      validateAttrValueType(schema) {
        let attributes = schema.attributes;
        let schemaAttributesKeys = Object.keys(attributes);
        let errorsArr = [];
        for (const attr of new Set(schemaAttributesKeys)) {
          let value = this.$attributes[attr];
          if (attributes[attr].type === 'string') {
            this.$attributes[attr] = Parser.parse('string', value);
          } else if (attributes[attr].type === 'number') {
            this.$attributes[attr] = Parser.parse('number', value);
          } else if (attributes[attr].type === 'boolean') {
            this.$attributes[attr] = Parser.parse('boolean', value);
          }
          /* if (typeof value !== attributes[attr].type) {
           this.$attributes[attr] = Parser.parse(attributes[attr].type, value);
           } */
          if (
            typeof value === 'undefined' ||
            (this.$attributes[attr] !== null &&
              typeof this.$attributes[attr] !== attributes[attr].type)
          ) {
            errorsArr.push(attr);
          }
        }
    
        if (errorsArr.length > 0) {
          let error = `Invalid type from attribute(s): ${errorsArr.toString()}`;
          throw new Error(error);
        }
    
        return true;
      }
}

module.exports = Schema
