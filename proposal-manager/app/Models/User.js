'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Schema = use('App/Models/Abstract/Schema');

class User extends Schema {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  static get hidden () {
    return ['password'];
  }

  static schema() {
    return {
      attributes: {
        email:          {type: 'string', required: true},
        password:       {type: 'string', required: true},
        first_name:     {type: 'string', required: true},
        last_name:      {type: 'string', required: true},
        password_reset: {type: 'string'},
        active:         {type: 'boolean', required: true},
        image:          {type: 'string'},
        roles:          {type: 'object', default: []},
      },
    };
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  /**
   * Create new users
   * @param data {object} - user properties
   * @returns {User} - user instance
   */
  static async createNewUser(data) {
    let {
      email,
      password,
      first_name,
      last_name,
      image,
      roles,
    } = data;

    data = {
      email: email ? email.trim() : email,
      password: await Hash.make(password),
      first_name: first_name,
      last_name: last_name,
      password_reset: null,
      active: true,
      image: image || null,
      roles: roles,
    };

    let user = await User.where({email: data.email}).first();
    if (user) {
      return {message: 'This user already exists! Choose another email.'};
    }

    user = new User(data);
    await user.save();
    return user;
  }
}

module.exports = User;
