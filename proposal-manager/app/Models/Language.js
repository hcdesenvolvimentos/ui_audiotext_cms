'use strict'
const Parser = require('@montacasa/parser');
/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Schema = use('App/Models/Abstract/Schema');

class Language extends Schema {
    static schema() {
        return {
            attributes: {
                code: {type: 'string', required: true},
                name: {type: 'string', required: true},
            },
        };
    }

    /**
     * Create new Language
     * @param {any} params - parameters object
     */
    static async createLanguage(params) {
        const { code, name } = params;
        return await this.findOrCreate({code}, {
            code: code,
            name: name, 
        });
    }
}

module.exports = Language;
