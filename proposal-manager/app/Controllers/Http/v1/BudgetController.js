'use strict'
const Budget = use('App/Models/Budget');
const Notifier = use ('App/Models/Support/Notifier');
class BudgetController {

    apiGetMeetingChannels() {
        return Budget.getMeetingChannels();
    }

    async apiGenerateProposals({ request, response }) {
        try {
            const {sessionCode, csrfToken} = request.params;
            const budget = await Budget.getBySession(sessionCode);
            await budget.updateBudget(request.all());
            await budget.loadRelations();
            console.log('Validating budget...');
            const validation = await Budget.validateBudget(budget.toJSON());
            if (!validation.hasErrors) {
              console.log('Generating proposals...');
              await budget.generateProposals();
              console.log('Sending proposals email...');
              console.log(budget.toJSON());
              await budget.sendProposals();
              return validation;
            }
            return validation;
        } catch(error) {
          console.error(error.message);
          const notifier = new Notifier();
          await notifier.alert('Evento: Envio de Email', {
            message: error.message,
          });
          return response.status(400).send(error.message);
        }
    }

    async apiGetProposals({ request, response }) {
        try {
          const {sessionCode, csrfToken} = request.params;
          const relations = ['service', 'finality', 'language', 'proposals'];
          console.log('Get proposals!');
          const budget = await Budget
            .query()
            .where({sessionCode/*status: Budget.SENT*/})
            .with(relations)
            .sort({created_at: -1})
            .first();

            return await budget.getPrettyFormat();
        } catch(error) {
            console.log(error);
        }
        return response.status(500).send();
    }

    async apiGetBudgetForm ({ request, response }) {
        try {
          const { utmSource, utmMedium, utmCampaign, utmTerm, utmContent } = request.all();
          const { sessionCode, csrfToken } = request.params;
          const {
            'user-agent': userAgent,
            'x-forwarded-for': ip,
          } = request.headers();
          const params = {
            utmSource: utmSource,
            utmMedium: utmMedium,
            utmCampaign: utmCampaign,
            utmTerm: utmTerm,
            utmContent: utmContent,
            ip: ip || request.ip(),
            userAgent: userAgent,
          };
          const budget = await Budget.getBySession(sessionCode, params);
          await budget.load(['proposals']);
          return response.json({budget, sessionCode, csrfToken})
        } catch(error) {
            console.log(error);
        }
        return response.status(500).send();
    }

    async apiSaveForm({ request, response }) {
        try {
            const {sessionCode, csrfToken} = request.params;
            let budget = await Budget.getBySession(sessionCode);
            await budget.updateBudget(request.all());
            return response.json({budget, sessionCode, csrfToken})
        } catch(error) {
          console.log(error);
        }
        return response.status(500).send();
    }

  async getEmailPreview({ request, response, view }) {
    try {
      const {sessionCode} = request.all();
      const relations = ['service', 'finality', 'language', 'proposals'];
      const budget = await Budget
        .query()
        .where({sessionCode/*status: Budget.SENT*/})
        .with(relations)
        .sort({created_at: -1})
        .first();
      if (budget) {
        return view.render('budget.email', {
          budget: budget.toJSON(),
        });
      }
      return response.status(403).json({message: 'Forbidden!'});
    } catch(error) {
      console.log(error);
    }
    return response.status(500).send();
  }

  async apiGetAllBudgets({ request }) {
    try {
      const {page, perPage} = request.all();
      const relations = ['service', 'finality', 'language', 'proposals'];
      const query = Budget.query();
      return await
        query
          .with(relations)
          .sort({created_at: -1})
          .where('username')
          .ne(null)
          .where('email')
          .ne(null)
          .paginate(Number(page) || 1, Number(perPage) || 25);
    } catch (error) {
      console.error(error.message);
    }
  }
}

module.exports = BudgetController;
