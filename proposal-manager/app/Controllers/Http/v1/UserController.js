'use strict'
const User = use('App/Models/User');

class UserController {
  async signIn ({ request, response, auth }) {
    try {
      const jwt = auth.authenticator('jwt');
      const {email, password} = request.all();
      const user = await User.where({email}).first();
      if (user) {
        const data = {};
        const attempt = await jwt.attempt(user.email, password);
        Object.assign(data, {accessToken: attempt.token});
        Object.assign(data, {user: user.toJSON()});
        return response.json(data);
      }
      return response.unprocessableEntity({message: 'Unprocessable Entity!'});
    } catch (error) {
      console.error(error.message);
    }
    return response.unauthorized({message: 'Invalid credentials!'});
  }

  async isLoggedIn ({ request, response, auth }) {
    try {
      const jwt = auth.authenticator('jwt');
      const isLoggedIn = await jwt.check();
      return response.json(isLoggedIn);
    } catch (error) {
      console.error(error.message);
    }
    return response.unauthorized({message: 'Invalid credentials!'});
  }

  async getCurrentUser ({ request, response, auth }) {
    try {
      const jwt = auth.authenticator('jwt');
      const isLoggedIn = await jwt.check();
      if (isLoggedIn) {
        const user = await jwt.getUser();
        return response.json(user);
      }
    } catch (error) {
      console.error(error.message);
    }
    return response.unauthorized({message: 'Invalid credentials!'});
  }
}

module.exports = UserController;
