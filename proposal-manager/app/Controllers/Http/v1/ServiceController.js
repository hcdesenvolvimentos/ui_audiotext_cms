'use strict'
const Service = use('App/Models/Service');
class ServiceController {
    async apiGetServices ({ request }) {
      try {
        const { enabled } = request.all();
        const query = Service.query();
        if (enabled) {
          query.where({enabled: true})
        }
        return await query.fetch();
      } catch (error) {
        console.error(error.message);
      }
    }
}

module.exports = ServiceController;
