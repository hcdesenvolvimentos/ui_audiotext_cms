'use strict'
const Finality = use('App/Models/Finality');
class FinalityController {
    async apiGetFinalities ({ response }) {
        return await Finality.all();
    }
}

module.exports = FinalityController
