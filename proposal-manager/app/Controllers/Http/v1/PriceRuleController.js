'use strict'
const PriceRule = use('App/Models/PriceRule');
class PriceRuleController {
    async apiGetPriceRules({ request, response }) {
      try {
        const {page, perPage, services} = request.all();
        const query = PriceRule.query();
        if (services && services.length > 0) {
          query.where('serviceCode').in(services);
        }
        return await
          query.with(['service'])
          .paginate(Number(page) || 1, Number(perPage) || 25);
      } catch (error) {
        console.error(error.message);
      }
      return response.internalServerError();
    }

    async apiUpdatePriceRules({ request, response }) {
      try {
        return await PriceRule.updatePriceRule(request.all());
      } catch (error) {
        console.error(error.message);
        return response.badRequest({message: error.message});
      }
    }

    async apiGetPriceRuleById({ request, response, params }) {
      try {
        return await PriceRule
          .query()
          .with(['service'])
          .where({_id: params.id})
          .first() || null;
      } catch (error) {
        console.error(error.message);
      }
      return response.internalServerError();
    }
}

module.exports = PriceRuleController;
