'use strict'
const Language = use('App/Models/Language');
class LanguageController {
    async apiGetLanguages ({ response }) {
        return await Language.all();
    }
}

module.exports = LanguageController;
