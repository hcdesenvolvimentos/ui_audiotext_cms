'use strict'

/*
|--------------------------------------------------------------------------
| FinalitySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Finality = use('App/Models/Finality');
class FinalitySeeder {
  async run () {
    await Finality.createFinality({name: 'Jurídica'});
    await Finality.createFinality({name: 'Acadêmica'});
    await Finality.createFinality({name: 'Elaboração de Ata'});
    await Finality.createFinality({name: 'Relatório'});
    await Finality.createFinality({name: 'Livro'});
    await Finality.createFinality({name: 'Legendagem'});
    await Finality.createFinality({name: 'Outro'});
  }
}

module.exports = FinalitySeeder;
