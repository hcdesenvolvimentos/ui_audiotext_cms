'use strict'

/*
|--------------------------------------------------------------------------
| LanguageSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Language = use('App/Models/Language');

class LanguageSeeder {
  async run () {
    await Language.createLanguage({code: 'pt-BR', name: 'Português'});
    await Language.createLanguage({code: 'en-US', name: 'Inglês'});
    await Language.createLanguage({code: 'es-ES', name: 'Espanhol'});
    await Language.createLanguage({code: 'other', name: 'Outro'});
  }
}

module.exports = LanguageSeeder;
