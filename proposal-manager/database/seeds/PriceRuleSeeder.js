'use strict'

/*
|--------------------------------------------------------------------------
| PriceRuleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const PriceRule = use('App/Models/PriceRule');

class PriceRuleSeeder {
  async run () {
    // truncate price rules
    /*
    await PriceRule.query().delete();
    await this.createTranscriptionRules();
    await this.createSubtitlingRules();
    await this.createSimpleTranslateRules();
    await this.createSwornTranslationRules();
    */
  }

  async createSwornTranslationRules() {
    // Tradução Juramentada	0-5	Por página	1					1	1	1
    await PriceRule.createRule({
      serviceCode: 'traducao-juramentada', 
      fixedPrice: false,
      minRange: 0,
      maxRange: 5,
      maxInstallment: 1,
      plans: [
        {name: 'instant', value: 60, deadline: 1},
        {name: 'express', value: 55, deadline: 1},
        {name: 'fast', value: 51, deadline: 1},
        {name: 'flex', value: 49, deadline: 1},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
      enabled: true,
    });
    // Tradução Juramentada	6-10	Por página	1					1	1	1
    await PriceRule.createRule({
      serviceCode: 'traducao-juramentada', 
      fixedPrice: false,
      minRange: 6,
      maxRange: 10,
      maxInstallment: 1,
      plans: [
        {name: 'instant', value: 55, deadline: 1},
        {name: 'express', value: 52, deadline: 1},
        {name: 'fast', value: 49, deadline: 1},
        {name: 'flex', value: 45, deadline: 1},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
      enabled: true,
    });
    // Tradução Juramentada	11+	Por página	2					1	1	1
    await PriceRule.createRule({
      serviceCode: 'traducao-juramentada', 
      fixedPrice: false,
      minRange: 11,
      maxRange: Infinity,
      maxInstallment: 2,
      plans: [
        {name: 'instant', value: 51, deadline: 1},
        {name: 'express', value: 47, deadline: 1},
        {name: 'fast', value: 45, deadline: 1},
        {name: 'flex', value: 39, deadline: 1},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
      enabled: true,
    });
  }

  async createSimpleTranslateRules() {
    // Tradução Simples	0-199	Fixa	1	100	/	/	/	1	1	1
    await PriceRule.createRule({
      serviceCode: 'traducao-simples', 
      fixedPrice: true,
      minRange: 0,
      maxRange: 199,
      maxInstallment: 1,
      plans: [
        {name: 'instant', value: 100, deadline: null},
        {name: 'express', value: null, deadline: null},
        {name: 'fast', value: null, deadline: null},
        {name: 'flex', value: null, deadline: null},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
      enabled: true,
    });
    // Tradução Simples	200-1499	Por palavra	1	0,4	0,38	0,36	0,34	1	1	1
    await PriceRule.createRule({
      serviceCode: 'traducao-simples', 
      fixedPrice: false,
      minRange: 200,
      maxRange: 1499,
      maxInstallment: 1,
      plans: [
        {name: 'instant', value: 0.4, deadline: null},
        {name: 'express', value: 0.38, deadline: null},
        {name: 'fast', value: 0.36, deadline: null},
        {name: 'flex', value: 0.34, deadline: null},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
      enabled: true,
    });
    // Tradução Simples	1500-2999	Por palavra	1	0,39	0,37	0,35	0,33	1	1	1
    await PriceRule.createRule({
      serviceCode: 'traducao-simples', 
      fixedPrice: false,
      minRange: 1500,
      maxRange: 2999,
      maxInstallment: 1,
      plans: [
        {name: 'instant', value: 0.39, deadline: null},
        {name: 'express', value: 0.37, deadline: null},
        {name: 'fast', value: 0.35, deadline: null},
        {name: 'flex', value: 0.33, deadline: null},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
      enabled: true,
    });
     // Tradução Simples	3000-5999	Por palavra	2	0,38	0,36	0,34	0,32	1	1	1
     await PriceRule.createRule({
      serviceCode: 'traducao-simples', 
      fixedPrice: false,
      minRange: 3000,
      maxRange: 5999,
      maxInstallment: 2,
      plans: [
        {name: 'instant', value: 0.38, deadline: null},
        {name: 'express', value: 0.36, deadline: null},
        {name: 'fast', value: 0.34, deadline: null},
        {name: 'flex', value: 0.32, deadline: null},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
       enabled: true,
    });
     // Tradução Simples	6000-9999	Por palavra	3	0,37	0,35	0,33	0,31	1	1	1
     await PriceRule.createRule({
      serviceCode: 'traducao-simples', 
      fixedPrice: false,
      minRange: 6000,
      maxRange: 9999,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 0.37, deadline: null},
        {name: 'express', value: 0.35, deadline: null},
        {name: 'fast', value: 0.33, deadline: null},
        {name: 'flex', value: 0.31, deadline: null},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
       enabled: true,
    });
    // Tradução Simples	10000-14999	Por palavra	3	0,36	0,34	0,32	0,3	1	1	1
    await PriceRule.createRule({
      serviceCode: 'traducao-simples', 
      fixedPrice: false,
      minRange: 10000,
      maxRange: 14999,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 0.36, deadline: null},
        {name: 'express', value: 0.34, deadline: null},
        {name: 'fast', value: 0.32, deadline: null},
        {name: 'flex', value: 0.3, deadline: null},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
      enabled: true,
    });
    // Tradução Simples	15000-19999	Por palavra	4	0,35	0,33	0,31	0,29	1	1	1
    await PriceRule.createRule({
      serviceCode: 'traducao-simples', 
      fixedPrice: false,
      minRange: 15000,
      maxRange: 19999,
      maxInstallment: 4,
      plans: [
        {name: 'instant', value: 0.35, deadline: null},
        {name: 'express', value: 0.33, deadline: null},
        {name: 'fast', value: 0.31, deadline: null},
        {name: 'flex', value: 0.29, deadline: null},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
      enabled: true,
    });
    // Tradução Simples	20000-29999	Por palavra	4	0,34	0,32	0,3	0,28	1	1	1
    await PriceRule.createRule({
      serviceCode: 'traducao-simples', 
      fixedPrice: false,
      minRange: 20000,
      maxRange: 29999,
      maxInstallment: 4,
      plans: [
        {name: 'instant', value: 0.34, deadline: null},
        {name: 'express', value: 0.32, deadline: null},
        {name: 'fast', value: 0.3, deadline: null},
        {name: 'flex', value: 0.28, deadline: null},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
      enabled: true,
    });
    // Tradução Simples	30000-59999	Por palavra	5	0,33	0,31	0,29	0,27	1	1	1
    await PriceRule.createRule({
      serviceCode: 'traducao-simples', 
      fixedPrice: false,
      minRange: 30000,
      maxRange: 59999,
      maxInstallment: 5,
      plans: [
        {name: 'instant', value: 0.33, deadline: null},
        {name: 'express', value: 0.31, deadline: null},
        {name: 'fast', value: 0.29, deadline: null},
        {name: 'flex', value: 0.27, deadline: null},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
      enabled: true,
    });
    // Tradução Simples	60000+	Por palavra	1	0,32	0,3	0,28	0,26	1	1	1
    await PriceRule.createRule({
      serviceCode: 'traducao-simples', 
      fixedPrice: false,
      minRange: 60000,
      maxRange: Infinity,
      maxInstallment: 1,
      plans: [
        {name: 'instant', value: 0.32, deadline: null},
        {name: 'express', value: 0.3, deadline: null},
        {name: 'fast', value: 0.28, deadline: null},
        {name: 'flex', value: 0.26, deadline: null},
      ],
      legalProposeTax: 1,
      languageTax: 1,
      installmentTax: 1,
      enabled: true,
    });
  }

  async createSubtitlingRules() {
    // Legendagem	0-10	Fixa	1	100	/	/	/	1,5	1,8	1,15	1	/	/	/
    await PriceRule.createRule({
      serviceCode: 'legendagem', 
      fixedPrice: true,
      minRange: 0,
      maxRange: 10,
      maxInstallment: 2,
      plans: [
        {name: 'instant', value: 100, deadline: 1},
        {name: 'express', value: null, deadline: null},
        {name: 'fast', value: null, deadline: null},
        {name: 'flex', value: null, deadline: null},
      ],
      legalProposeTax: 1.5,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Legendagem	11-29	Fixa	1	228	190	165	133	2	1,8	1,15	1	2	4	7
    await PriceRule.createRule({
      serviceCode: 'legendagem', 
      fixedPrice: true,
      minRange: 11,
      maxRange: 29,
      maxInstallment: 2,
      plans: [
        {name: 'instant', value: 228, deadline: 1},
        {name: 'express', value: 190, deadline: 2},
        {name: 'fast', value: 165, deadline: 4},
        {name: 'flex', value: 133, deadline: 7},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Legendagem	30-59	Fixa	1	228	190	165	133	2	1,8	1,15	1	2	4	7
    await PriceRule.createRule({
      serviceCode: 'legendagem', 
      fixedPrice: true,
      minRange: 30,
      maxRange: 59,
      maxInstallment: 2,
      plans: [
        {name: 'instant', value: 228, deadline: 1},
        {name: 'express', value: 190, deadline: 2},
        {name: 'fast', value: 165, deadline: 4},
        {name: 'flex', value: 133, deadline: 7},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Legendagem	60-89	Por minuto	2	6,41	5,67	5,17	4,55	2	1,8	1,15	1	2	4	7
    await PriceRule.createRule({
      serviceCode: 'legendagem', 
      fixedPrice: false,
      minRange: 60,
      maxRange: 89,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 6.41, deadline: 1},
        {name: 'express', value: 5.67, deadline: 2},
        {name: 'fast', value: 5.17, deadline: 4},
        {name: 'flex', value: 4.55, deadline: 7},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Legendagem	90-179	Por minuto	3	6,41	5,67	5,17	4,55	2	1,8	1,15	2	3	6	9
    await PriceRule.createRule({
      serviceCode: 'legendagem', 
      fixedPrice: false,
      minRange: 90,
      maxRange: 179,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 6.41, deadline: 2},
        {name: 'express', value: 5.67, deadline: 3},
        {name: 'fast', value: 5.17, deadline: 6},
        {name: 'flex', value: 4.55, deadline: 9},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Legendagem	180-359	Por minuto	3	6,41	5,67	5,17	4,55	2	1,8	1,15	3	5	8	11
    await PriceRule.createRule({
      serviceCode: 'legendagem', 
      fixedPrice: false,
      minRange: 180,
      maxRange: 359,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 6.41, deadline: 3},
        {name: 'express', value: 5.67, deadline: 5},
        {name: 'fast', value: 5.17, deadline: 8},
        {name: 'flex', value: 4.55, deadline: 11},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Legendagem	360-719	Por minuto	4	6,41	5,67	5,17	4,55	2	1,8	1,15	5	8	11	14
    await PriceRule.createRule({
      serviceCode: 'legendagem', 
      fixedPrice: false,
      minRange: 360,
      maxRange: 719,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 6.41, deadline: 5},
        {name: 'express', value: 5.67, deadline: 8},
        {name: 'fast', value: 5.17, deadline: 11},
        {name: 'flex', value: 4.55, deadline: 14},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Legendagem	720-1439	Por minuto	4	6,41	5,67	5,17	4,55	2	1,8	1,15	8	12	15	18
    await PriceRule.createRule({
      serviceCode: 'legendagem', 
      fixedPrice: false,
      minRange: 720,
      maxRange: 1439,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 6.41, deadline: 8},
        {name: 'express', value: 5.67, deadline: 12},
        {name: 'fast', value: 5.17, deadline: 15},
        {name: 'flex', value: 4.55, deadline: 18},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Legendagem	1500-2999	Por minuto	5	6,41	5,67	5,17	4,55	2	1,8	1,15	12	16	19	22
    await PriceRule.createRule({
      serviceCode: 'legendagem', 
      fixedPrice: false,
      minRange: 1500,
      maxRange: 2999,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 6.41, deadline: 12},
        {name: 'express', value: 5.67, deadline: 16},
        {name: 'fast', value: 5.17, deadline: 19},
        {name: 'flex', value: 4.55, deadline: 22},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Legendagem	3000+	Por minuto	Customizado											
    await PriceRule.createRule({
      serviceCode: 'legendagem', 
      fixedPrice: false,
      minRange: 3000,
      maxRange: Infinity,
      maxInstallment: 1,
      plans: [
        {name: 'instant', value: 1, deadline: 1},
        {name: 'express', value: 1, deadline: 1},
        {name: 'fast', value: 1, deadline: 1},
        {name: 'flex', value: 1, deadline: 1},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
  }

  async createTranscriptionRules() {
    // Transcrição	0-10	Fixa	1	100	/	/	/	1,5	1,8	1,15	1	/	/	/
    await PriceRule.createRule({
      serviceCode: 'transcricao', 
      fixedPrice: true,
      minRange: 0,
      maxRange: 10,
      maxInstallment: 2,
      plans: [
        {name: 'instant', value: 100, deadline: 1},
        {name: 'express', value: null, deadline: null},
        {name: 'fast', value: null, deadline: null},
        {name: 'flex', value: null, deadline: null},
      ],
      legalProposeTax: 1.5,
      languageTax: 1.2,
      installmentTax: 1.15,
      enabled: true,
    });
    // Transcrição	11-29	Fixa	1	228	190	165	133	2	1,8	1,15	1	2	4	7
    await PriceRule.createRule({
      serviceCode: 'transcricao',
      fixedPrice: true,
      minRange: 11,
      maxRange: 29,
      maxInstallment: 2,
      plans: [
        {name: 'instant', value: 197, deadline: 1},
        {name: 'express', value: 164, deadline: 2},
        {name: 'fast', value: 129, deadline: 4},
        {name: 'flex', value: 100, deadline: 7},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Transcrição	30-59	Fixa	1	228	190	165	133	2	1,8	1,15	1	2	4	7
    await PriceRule.createRule({
      serviceCode: 'transcricao', 
      fixedPrice: true,
      minRange: 30,
      maxRange: 59,
      maxInstallment: 2,
      plans: [
        {name: 'instant', value: 271, deadline: 1},
        {name: 'express', value: 227, deadline: 2},
        {name: 'fast', value: 189, deadline: 4},
        {name: 'flex', value: 139, deadline: 7},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Transcrição	60-89	Por minuto	2	4,41	3,67	3,17	2,55	2	1,8	1,15	1	2	4	7
    await PriceRule.createRule({
      serviceCode: 'transcricao', 
      fixedPrice: false,
      minRange: 60,
      maxRange: 89,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 4.5, deadline: 1},
        {name: 'express', value: 3.8, deadline: 2},
        {name: 'fast', value: 3.2, deadline: 4},
        {name: 'flex', value: 2.4, deadline: 7},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Transcrição	90-179	Por minuto	3	4,41	3,67	3,17	2,55	2	1,8	1,15	2	3	6	9
    await PriceRule.createRule({
      serviceCode: 'transcricao', 
      fixedPrice: false,
      minRange: 90,
      maxRange: 179,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 4.4, deadline: 1},
        {name: 'express', value: 3.7, deadline: 3},
        {name: 'fast', value: 3.1, deadline: 6},
        {name: 'flex', value: 2.3, deadline: 10},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Transcrição	180-359	Por minuto	3	4,41	3,67	3,17	2,55	2	1,8	1,15	3	5	8	11
    await PriceRule.createRule({
      serviceCode: 'transcricao', 
      fixedPrice: false,
      minRange: 180,
      maxRange: 359,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 4.3, deadline: 1},
        {name: 'express', value: 3.6, deadline: 3},
        {name: 'fast', value: 3, deadline: 6},
        {name: 'flex', value: 2.2, deadline: 10},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Transcrição	360-719	Por minuto	4	4,41	3,67	3,17	2,55	2	1,8	1,15	5	8	11	14
    await PriceRule.createRule({
      serviceCode: 'transcricao', 
      fixedPrice: false,
      minRange: 360,
      maxRange: 719,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 4.2, deadline: 2},
        {name: 'express', value: 3.5, deadline: 5},
        {name: 'fast', value: 2.9, deadline: 8},
        {name: 'flex', value: 2.1, deadline: 12},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Transcrição	720-1439	Por minuto	4	4,41	3,67	3,17	2,55	2	1,8	1,15	8	12	15	18
    await PriceRule.createRule({
      serviceCode: 'transcricao', 
      fixedPrice: false,
      minRange: 720,
      maxRange: 1499,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 4.1, deadline: 3},
        {name: 'express', value: 3.4, deadline: 6},
        {name: 'fast', value: 2.8, deadline: 10},
        {name: 'flex', value: 2, deadline: 15},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Transcrição	1500-2999	Por minuto	5	4,41	3,67	3,17	2,55	2	1,8	1,15	12	16	19	22
    await PriceRule.createRule({
      serviceCode: 'transcricao', 
      fixedPrice: false,
      minRange: 1500,
      maxRange: 2999,
      maxInstallment: 3,
      plans: [
        {name: 'instant', value: 4, deadline: 4},
        {name: 'express', value: 3.3, deadline: 9},
        {name: 'fast', value: 2.7, deadline: 15},
        {name: 'flex', value: 1.9, deadline: 20},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
    // Transcrição	3000+	Por minuto	Customizado											
    await PriceRule.createRule({
      serviceCode: 'transcricao', 
      fixedPrice: false,
      minRange: 3000,
      maxRange: Infinity,
      maxInstallment: 1,
      plans: [
        {name: 'instant', value: 1, deadline: 1},
        {name: 'express', value: 1, deadline: 1},
        {name: 'fast', value: 1, deadline: 1},
        {name: 'flex', value: 1, deadline: 1},
      ],
      legalProposeTax: 2,
      languageTax: 1.8,
      installmentTax: 1.15,
      enabled: true,
    });
  }
}

module.exports = PriceRuleSeeder;
