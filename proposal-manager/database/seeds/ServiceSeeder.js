'use strict'

/*
|--------------------------------------------------------------------------
| ServiceSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Service = use('App/Models/Service');

class ServiceSeeder {
  async run () {
    await Service.query().delete();
    // Create transcription service
    await Service.createService({
      name: 'Transcrição',
      metric: Service.MINUTE,
      fixedRange: '0-59',
      metricRange: '60-2999',
      customRange: '3000-*',
      enabled: true,
    });

    // Create subtitling service
    await Service.createService({
      name: 'Legendagem',
      metric: Service.MINUTE,
      fixedRange: '0-10',
      metricRange: '11-2999',
      customRange: '3000-*',
      enabled: false,
    });

    // Create simple translation service
    await Service.createService({
      name: 'Tradução Simples',
      metric: Service.WORD,
      fixedRange: '0-199',
      metricRange: '200-59999',
      customRange: '60000-*',
      enabled: false,
    });

    // Create sworn translation service
    await Service.createService({
      name: 'Tradução Juramentada',
      metric: Service.PAGE,
      fixedRange: '0-5',
      metricRange: '6-10',
      customRange: '11-*',
      enabled: false,
    });
  }
}

module.exports = ServiceSeeder;
