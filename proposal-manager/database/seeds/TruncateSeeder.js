'use strict'

/*
|--------------------------------------------------------------------------
| ServiceSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Budget = use('App/Models/Budget');
const Proposal = use('App/Models/Proposal');

class ServiceSeeder {
  async run () {
    // await Proposal.query().delete();
    // await Budget.query().delete();
  }
}

module.exports = ServiceSeeder;
