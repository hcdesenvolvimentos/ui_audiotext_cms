'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const User = use('App/Models/User');

class UserSeeder {
  async run () {
    const data = {
      email: 'admin@audiotext.com.br',
      password: '4ud10Text',
      first_name: 'Admin',
      last_name: 'Account',
      password_reset: null,
      active: true,
      image: null,
      roles: [],
    };
    await User.createNewUser(data);
  }
}

module.exports = UserSeeder;
