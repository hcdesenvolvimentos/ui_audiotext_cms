const { ServiceProvider } = require('@adonisjs/fold');
const RabbitMQProducer = require('./RabbitMQProducer');

class RabbitMQProvider extends ServiceProvider {
  register() {
    this.app.singleton('RabbitMQ/Queue/Producer', () => {
      const Config = this.app.use('Adonis/Src/Config');
      const Logger = this.app.use('Logger');

      const producer = new RabbitMQProducer(Config, Logger);

      producer.connect(() => { producer.logger.info(`[AMQP ${producer.type}] Waiting for work...`) });

      return producer
    })
  }
}

module.exports = RabbitMQProvider;
