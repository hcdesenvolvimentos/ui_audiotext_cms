const RabbitMQService = require('./RabbitMQService');

class RabbitMQProducer extends RabbitMQService {

  constructor(Config, logger) {
    super(Config, logger, 'Producer');
    this._offlinePubQueue = [];
    this.cbOnReconnet = this.startPublisher;
  }

  async publish(exchange, content, options) {
    try {

      await this._pubChannel.sendToQueue(exchange, Buffer.from(content), options, err => {
        if (err) {
          this.logger.error(err);
        }
      });

      await this._pubChannel.waitForConfirms(err => {
        if (err) {
          this.logger.error(err);
        }
      });

      this.logger.info(" [x] Sent '%s'", content);
    } catch (e) {
      this.logger.error(`[AMQP ${this.type}] channel publish failure: `, e.message);
      this._offlinePubQueue.push([exchange, content, options]);
      throw e;
    }
  }

  async sendPending() {
    if (this._connection) {
      try {
        if (this._offlinePubQueue.length > 0) {
          const [exchange, routingKey, content] = this._offlinePubQueue.shift();
          this.publish(exchange, routingKey, content);
        }
        setTimeout(() => this.sendPending(), 5000);
      } catch (e) {
        this.logger.error(e.message);
      }
    }
  }

  async startPublisher() {
    try {
      this._connection.createConfirmChannel((err, ch) => {
        if (this.closeOnErr(err)) return;

        ch.on('ack', err => {
          this.logger.info(err);
        });

        ch.on('nack', err => {
          this.logger.info(err);
        });

        ch.on('error', err => {
          this.logger.error(`[AMQP ${this.type}] channel error`, err.message)
        });

        ch.on('close', () => {
          this.logger.info(`[AMQP ${this.type}] channel closed`);
        });

        this._pubChannel = ch;

        this.sendPending();
      })
    } catch (e) {
      this.logger.error(e.message);
    }
  }
}

module.exports = RabbitMQProducer;
