const amqp = require('amqplib/callback_api');
const TEN_SECONDS = 10000;

class RabbitMQService {
  constructor(Config, logger, type) {
    this.amqpUrl = Config.get('queues.rabbitmq.url');
    this.type = type;
    this.logger = logger;
    this.logger.info(`[AMQP ${type}] initializing...`);
    this.cbOnReconnet = null;
  }

  async connect(onEnd) {
    this.logger.info(`[AMQP ${this.type}] Trying to connect to ${this.amqpUrl}`);
    amqp.connect(this.amqpUrl, {}, (err, connection) => {
      if (err) {
        this.logger.info(err)
        return setTimeout(() => this.connect(onEnd), TEN_SECONDS)
      }

      this.logger.info(`[AMQP ${this.type}] connection established`);
      this._connection = connection;
      this.initConnection();

      return onEnd();
    })
  }

  initConnection() {
    this._connection.on('error', (err) => {
      if (err && err.message !== 'Connection closing') {
        this.logger.error(`[AMQP ${this.type}] conn error ${err.message}`)
      }
      this.logger.info(err);
    });

    this._connection.on('close', () => {
      this.logger.error('[AMQP] reconnecting');
      return setTimeout(() => this.connect(() => {
        this.logger.info('[AMQP] ended');
      }), 1000)
    });

    if (this.isCallable(this.cbOnReconnet)) {
      this.logger.info(`[AMQP ${this.type}] call ${this.cbOnReconnet.name}() callback function`);
      this.cbOnReconnet();
    }
  }

  isCallable(fn) {
    return this._connection && typeof fn === 'function';
  }

  closeOnErr(err) {
    if (!err) return false;
    this.logger.error('[AMQP] error', err);
    this._connection.close();
    return true
  }
}

module.exports = RabbitMQService
