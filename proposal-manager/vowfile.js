'use strict'
// https://forum.adonisjs.com/t/run-test-with-env-node-testing/1337/3
// https://michaelzanggl.com/articles/testing-made-easy-with-adonisjs/
// https://www.codementor.io/@katifrantz796/how-to-build-rest-apis-with-tdd-and-adonis-js-s76hshvyh
// https://www.mikealche.com/software-development/how-to-set-up-a-testing-for-adonisjs-apps
const ace = require('@adonisjs/ace')

module.exports = (cli, runner) => {
  runner.before(async () => {
    use('Adonis/Src/Server').listen(process.env.HOST, process.env.PORT)

    //await ace.call('migration:run', {}, { silent: true })
  })

  runner.after(async () => {
    use('Adonis/Src/Server').getInstance().close()

    //await ace.call('migration:reset', {}, { silent: true })
  })
}
