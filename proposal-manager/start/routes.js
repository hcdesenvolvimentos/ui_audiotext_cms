'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {import('@adonisjs/framework/src/Route/Manager'} */
const Route = use('Route');
const Notifier = use ('App/Models/Support/Notifier');

Route.get('/', ({ response }) => {
  return {message: 'Audiotext Api (:'};
});

Route.group('Version 1 Group', () => {
    Route.get('email/preview', 'v1/BudgetController.getEmailPreview');
}).prefix('v1');

Route.group('Version 1 Group', () => {
    Route.get('services', 'v1/ServiceController.apiGetServices');
    Route.get('finalities', 'v1/FinalityController.apiGetFinalities');
    Route.get('meeting-channels', 'v1/BudgetController.apiGetMeetingChannels');
    Route.get('languages', 'v1/LanguageController.apiGetLanguages');
}).prefix('v1');

Route.group('Version 1 Group', () => {
  Route.patch('budget', 'v1/BudgetController.apiSaveForm');
  Route.get('budget', 'v1/BudgetController.apiGetBudgetForm');
  Route.get('budget/proposals', 'v1/BudgetController.apiGetProposals');
  Route.post('budget/proposals', 'v1/BudgetController.apiGenerateProposals');
  Route.post('budget/proposals/send/:plan', 'v1/BudgetController.apiSendChosenProposal');
}).middleware('budget-session').prefix('v1');

Route.group(() => {
  Route.post('/sign-in', 'v1/UserController.signIn');
  Route.post('/is-authenticated', 'v1/UserController.isLoggedIn');
  Route.post('/user', 'v1/UserController.getCurrentUser');
}).prefix('v1/auth');

Route.group(() => {
  Route.get('rules', 'v1/PriceRuleController.apiGetPriceRules');
  Route.get('rules/:id', 'v1/PriceRuleController.apiGetPriceRuleById');
  Route.patch('rules', 'v1/PriceRuleController.apiUpdatePriceRules');
  Route.get('budgets', 'v1/BudgetController.apiGetAllBudgets');
}).middleware(['auth:jwt']).prefix('v1');

Route.get('publish', ({ request, response }) => {
  const message = {'message': 'Hello World!'};
  new Notifier().publish(message);
  return response.status(200).send(message);
});
