app.tracking = {
    cookieKey: 'audiotext-budget-tracking',
    queryParams: ['utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content'],
    agent: {
        referrer: '',
        utm_source: 'Direct',
        utm_medium: 'none',
        utm_campaign: 'none',
        utm_term: 'none',
        utm_content: 'none',
    },
    run: function () {
        var referrer = document.referrer;
        var url = window.location.search;
        if (!referrer.trim()) {
            return app.tracking.buildUtms(referrer, 'Direct', url);
        }
        else if (referrer.match(/google\./)) {
            return app.tracking.buildUtms(referrer, 'Google', url);
        }
        else if (referrer.match(/facebook\./)) {
            return app.tracking.buildUtms(referrer, 'Facebook', url);
        }
        else if (referrer.match(/instagram\./)) {
            return app.tracking.buildUtms(referrer, 'Instagram', url);
        }
        else if (referrer.match(/pinterest\./)) {
            return app.tracking.buildUtms(referrer, 'Pinterest', url);
        }
        else if (referrer.match(/twitter\./)) {
            return app.tracking.buildUtms(referrer, 'Twitter', url);
        }
        else if (referrer.match(/slack\./)) {
            return app.tracking.buildUtms(referrer, 'Slack', url);
        }
        else {
            return this.buildUtms(referrer, 'Referrer', url);
        }
    },

    buildUtms: function (referrer, utmSource, url) {
        app.tracking.agent.referrer = referrer;
        app.tracking.agent.utm_source = utmSource || 'none';
        app.tracking.queryParams.forEach(function (param) {
            url.replace(/\?/, '').split('&').filter(function (p) {
                if (p.indexOf(param) > -1) {
                    app.tracking.agent[param] = p.split('=')[1] || null;
                }
                if (utmSource === 'Google') {
                    app.tracking.agent.utm_source = 'Google';
                    app.tracking.agent.utm_medium = 'Organic';
                }
                // Override utm_source and utm_medium if found gclid param
                if (utmSource === 'Google' && p.indexOf('gclid') > -1) {
                    app.tracking.agent.utm_medium = 'Google Ads';
                }
            });
        });

        // Extract referrer domain if it exists and set source
        if (app.tracking.agent.utm_source === 'Referrer' && referrer) {
            app.tracking.agent.utm_source = app.tracking.extractHostname(referrer);
        }

        /*
        Conditions:
        1. Exists referrer and utmSource !== Direct
        Or: 2. No exists referrer and utmSource !== Direct
        Or: 3. No exists tracking cookie registered yet
         */
        if (
            (app.tracking.agent.referrer && app.tracking.agent.utm_source.trim() !== 'Direct' ) ||
            (!app.tracking.agent.referrer && app.tracking.agent.utm_source.trim() !== 'Direct') ||
            !app.cookies.get(app.tracking.cookieKey)
        ) {
                app.cookies.remove(app.tracking.cookieKey);
            var joinUtms = function () {
                return app.tracking.agent.utm_source
                    + '|' + app.tracking.agent.utm_medium
                    + '|' + app.tracking.agent.utm_campaign
                    + '|' + app.tracking.agent.utm_term
                    + '|' + app.tracking.agent.utm_content
            };
            app.cookies.set(app.tracking.cookieKey, joinUtms());
        }

    },

    extractHostname: function extractHostname(url) {
        var hostname = '';
        //find & remove protocol (http, ftp, etc.) and get hostname
        if (url.indexOf("//") > -1) {
            hostname = url.split('/')[2];
        }
        else {
            hostname = url.split('/')[0];
        }
        //find & remove port number
        hostname = hostname.split(':')[0];
        //find & remove "?"
        hostname = hostname.split('?')[0];
        return hostname;
    }
};
