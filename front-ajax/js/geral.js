$(document).ready(function (){
    app.tracking.run();
    app.api.services.get();
    app.api.finalities.get();
    app.api.languages.get();
    app.api.budget.getMeetingChannels();
    app.api.budget.get()
        .then(function (data) {
            fillForm(data);
        });

    $(".advanceSteps").click(function(e) {
        var form = $(e.target).parent('form');
        var formId = form.attr('id');
        var isValid = app.api.validator.validateForm(form);
        if (!isValid) {
            e.preventDefault();
        } else {
            if (formId === 'primeiraEtapa') {
                nextStep(e);
            } else {
                var isCaptchaChecked = app.api.captcha.isChecked();
                if (isCaptchaChecked) {
                    $(this).css("background", "#6876ab");
                    $(this).text('Gerando Propostas...');
                    // Generate proposals
                    generateProposals(e);
                } else {
                    alert('Por favor, selecione a opção: Não sou um robô')
                }
            }
        }
    });

    $("#btnVisualizarOrcamento").click(function(e) {
        showProposals();
    });

	$(".pg-inicial .botaoEnviarArquivos, li.itemSolicitarOcamento a").click(function(e) {
        app.popoup.resetSteps();
        app.api.budget.get()
            .then(function (data) {
                fillForm(data);
                app.popoup.open();
            });
        $('.pop-up').fadeIn();
        $('body').addClass('travarScroll');
    });

    $("#voltarParaOForm").click(function(e) {
        app.api.budget.get()
            .then(function (data) {
                fillForm(data);
                app.popoup.updateBodyTo('form');
            });
    });

    $(".close-popup").click(function(e) {
        $('body').removeClass('travarScroll');
        app.cookies.remove('audiotext-budget-session');
        app.popoup.close();
    });

    window.onclick = function(e) {
        if (e.target.getAttribute('id') === 'budget-pop-up') {
            app.popoup.close();
            $('body').removeClass('travarScroll');
        }
    }

    $("#voltar").click(function(e) {
        backStep(e)
    });

    $("#budget-service").change(function (e) {
        app.api.validator.enableParticipantsAmountField($(e.target).val());
    });

    function fillForm(data) {
        $('#budget-csrf').val(data.csrfToken);
        $.each(Object.keys(data.budget), function(index, key) {
            if (key === 'serviceCode' || key === 'finalityCode' || key === 'languageCode' || key === 'howDidMeetUs') {
                var option = data.budget[key];
                if (option) {
                    $('select[name*='+key+'] option[value="'+ option + '"]').attr('selected','selected');
                }
            } else if (key === 'isWhatsApp') {
                document.querySelector('#budget-isWhatsApp').checked = !!data.budget[key];
            } else {
                var e = $('#budget-'+key);
                if (e) {
                    e.val(data.budget[key] === 0 ? '' : data.budget[key]);
                }
            }
        });
        app.api.validator.enableParticipantsAmountField(data.budget.serviceCode, data.budget.participantsAmount || '');
    }

    function serializeToJSON(fields) {
        var b = {};
        b.sessionCode = app.cookies.get("audiotext-budget-session");
        $.each(fields, function(index) {
            b[fields[index].name] = fields[index].value;
        });

        if ($('#budget-isWhatsApp').is(":visible")) {
            b['isWhatsApp'] = $('#budget-isWhatsApp').is(":checked");
        }

        return b;
    }

    function saveForm(e) {
        var form = $(e.target).parent('form');
        // Patch messages.json.json data
        return app.api.budget.patch(serializeToJSON(form.serializeArray()));
    }

    function backStep(e) {
        // Save form
        saveForm(e);
        setTimeout(function(){
            $("#primeiraEtapa").slideDown();
        }, 500);
        $("#segundaEtapa").fadeOut();
        e.preventDefault();
    }

    function nextStep(e) {
        // Save form
        saveForm(e);
        $("#primeiraEtapa").fadeOut();
        setTimeout(function(){
            $("#segundaEtapa").slideDown();
        }, 500);
        e.preventDefault();
    }

    function showProposals(sessionCode) {
        app.api.proposals.get(sessionCode);
        app.popoup.updateBodyTo('proposals');
    }

    function generateProposals(e) {
        // Save form
        saveForm(e)
            .then(function (budget) {
                app.api.proposals.generate(budget)
                    .then(function (response) {
                        if (!response.hasErrors) {
                            app.api.captcha.reset();
                            showProposals(budget.sessionCode);
                        }
                    });
            });

        e.preventDefault();
    }

    $("#budget-phone").bind('input propertychange',function(){
        // pego o valor do telefone
        var texto = $(this).val();
        // Tiro tudo que não é numero
        texto = texto.replace(/[^\d]/g, '');
        // Se tiver alguma coisa
        if (texto.length > 0)
        {
            // Ponho o primeiro parenteses do DDD
            texto = "(" + texto;

            if (texto.length > 3)
            {
                // Fecha o parenteses do DDD
                texto = [texto.slice(0, 3), ") ", texto.slice(3)].join('');
            }
            if (texto.length > 12)
            {
                // Se for 13 digitos ( DDD + 9 digitos) ponhe o traço no quinto digito
                if (texto.length > 13)
                    texto = [texto.slice(0, 10), "-", texto.slice(10)].join('');
                else
                // Se for 12 digitos ( DDD + 8 digitos) ponhe o traço no quarto digito
                    texto = [texto.slice(0, 9), "-", texto.slice(9)].join('');
            }
            // Não adianta digitar mais digitos!
            if (texto.length > 15)
                texto = texto.substr(0,15);
        }
        // Retorna o texto
        $(this).val(texto);
    });
});
