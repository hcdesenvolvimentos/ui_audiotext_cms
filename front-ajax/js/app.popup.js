app.popoup = {
    close: function () {
        $('#budget-pop-up').css('display', 'none');
    },

    open: function () {
        app.popoup.updateBodyTo('form');
        $('#budget-pop-up').css('display', 'block');
    },

    resetSteps: function () {
        app.api.services.get();
        app.api.finalities.get();
        app.api.languages.get();
        app.api.budget.getMeetingChannels();
        $("#segundaEtapa").css('display', 'none');
        $("#primeiraEtapa").css('display', 'block');
    },

    updateBodyTo: function (container) {
        var popupContent = $('#budget-pop-up .content');
        var formContainer = $('#form-container');
        var proposalContainer = $('#proposal-container');
        switch(container) {
            case 'form':
                formContainer.css('display', 'block');
                proposalContainer.css('display', 'none');
                popupContent.css('width', '650px');
                break;
            case 'proposals':
                formContainer.css('display', 'none');
                proposalContainer.css('display', 'block');
                popupContent.css('width', '95%');
                break;
            default:
                console.log('container not found!');
        }
    }
};