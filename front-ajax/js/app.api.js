app.api = {
    endpoint: 'http://dev.api.audiotext.com.br/v1',
    budget: {
        patch: function (budget) {
           return $.ajax({
              url: app.api.endpoint + '/budget',
              method: 'PATCH',
              timeout: 10000,
              dataType: 'json',
              contentType: 'application/json',
              headers: {'X-XSRF-TOKEN': budget._csrf || null},
              data: JSON.stringify(budget),
              async: false,
              success: function (data) {
                  // console.info(data);
              },
              error: function(message)
              {
                  console.error(message.responseText);
              }
           });
        },
        get: function () {
            var sessionCode = app.cookies.get("audiotext-budget-session");
            var utms = app.cookies.get(app.tracking.cookieKey);
            var utmSource = '';
            var utmMedium = '';
            var utmCampaign = '';
            var utmTerm = '';
            var utmContent = '';

            if (utms) {
                utmSource = utms.split('|')[0] || 'Direct';
                utmMedium = utms.split('|')[1] || 'none';
                utmCampaign = utms.split('|')[2] || 'none';
                utmTerm = utms.split('|')[3] || 'none';
                utmContent = utms.split('|')[4] || 'none';
            }

            // Get
            var params = sessionCode ? '?sessionCode='+sessionCode : '?sessionCode=';
            params += utmSource ? '&utmSource='+utmSource : '&utmSource=';
            params += utmMedium ? '&utmMedium='+utmMedium : '&utmMedium=';
            params += utmCampaign ? '&utmCampaign='+utmCampaign : '&utmCampaign=';
            params += utmTerm ? '&utmTerm='+utmTerm : '&utmTerm=';
            params += utmContent ? '&utmContent='+utmContent : '&utmContent=';

            return $.ajax({
                url: app.api.endpoint + '/budget' +params,
                method: 'GET',
                timeout: 10000,
                dataType: 'json',
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    app.cookies.set('audiotext-budget-session', data.sessionCode, 7);
                    // console.debug(document.referrer);
                },
                error: function(message)
                {
                    console.error(message);
                },
                complete: function (message) {
                }
            });
        },
        getMeetingChannels: function () {
            return $.ajax({
                url: app.api.endpoint + '/meeting-channels',
                method: 'GET',
                timeout: 10000,
                dataType: 'json',
                async: false,
                contentType: 'application/json',
                success: function (data) {
                    $('#budget-howDidMeetUs').text('');
                    $('#budget-howDidMeetUs').append('<option value="">Como nos conheceu?</option>');
                    $.each(data, function (i, option) {
                        $('#budget-howDidMeetUs').append($('<option>', {
                            value: option,
                            text : option,
                        }));
                    });
                },
                error: function(message)
                {
                    console.error(message);
                },
                complete: function (message) {

                }
            });
        }
    },
    services: {
        get: function () {
            $.ajax({
                url: app.api.endpoint + '/services?enabled=true',
                method: 'GET',
                timeout: 10000,
                dataType: 'json',
                contentType: 'application/json',
                async: false,
                success: function (data) {
                    $('#budget-service').text('');
                    $('#budget-service').append('<option value="">Selecione o Serviço</option>');
                    $.each(data, function (i, item) {
                        $('#budget-service').append($('<option>', {
                            value: item.code,
                            text : item.name,
                        }));
                    });
                },
                error: function(message)
                {
                    console.error(message.responseText);
                },
            });
        },
    },
    finalities: {
        get: function () {
            $.ajax({
                url: app.api.endpoint + '/finalities',
                method: 'GET',
                timeout: 10000,
                dataType: 'json',
                contentType: 'application/json',
                async: false,
                success: function (data) {
                    $('#budget-finality').text('');
                    $('#budget-finality').append('<option value="">Selecione a Finalidade</option>');
                    $.each(data, function (i, item) {
                        $('#budget-finality').append($('<option>', {
                            value: item.code,
                            text : item.name,
                        }));
                    });
                },
                error: function(message)
                {
                    console.error(message.responseText);
                },
            });
        }
    },
    languages: {
        get: function () {
            $.ajax({
                url: app.api.endpoint + '/languages',
                method: 'GET',
                timeout: 10000,
                dataType: 'json',
                contentType: 'application/json',
                async: false,
                success: function (data) {
                    $('#budget-language').text('');
                    $('#budget-language').append('<option value="">Selecione o Idioma</option>');
                    $.each(data, function (i, item) {
                        $('#budget-language').append($('<option>', {
                            value: item.code,
                            text : item.name,
                        }));
                    });
                },
                error: function(message)
                {
                    console.error(message.responseText);
                },
            });
        },
    },
    proposals: {
        generate: function (budget) {
            return $.ajax({
                url: app.api.endpoint + '/budget/proposals',
                method: 'POST',
                timeout: 10000,
                dataType: 'json',
                async: false,
                contentType: 'application/json',
                headers: {'X-XSRF-TOKEN': budget._csrf || null},
                data: JSON.stringify(budget),
                success: function (response) {
                    // Clear all error span labels
                    $('.formularioOrcamento > .validacao').removeClass('compoInvalido');
                    // Check by response form errors
                    if (response.hasErrors) {
                        // TODO - Display general message to notify errors
                        var errors = response.errors;
                        for (var i = 0; i < errors.length; i++) {
                            var key = Object.keys(errors[i])[0];
                            var message = errors[i][key].message;
                            var field = $("#validation-"+key);
                            field.addClass('compoInvalido');
                            field.text(message);
                        }
                        return false;
                    }
                    return true;
                },
                error: function(message)
                {
                    console.error(message);
                },
                complete: function (message) {
                    var btn = $('#gerarPropostas');
                    btn.css("background", "#2f3b6b");
                    btn.text('Avançar');
                }
            });
        },
        get: function (sessionCode) {
            $.ajax({
                url: app.api.endpoint + '/budget/proposals',
                method: 'GET',
                timeout: 10000,
                dataType: 'json',
                async: false,
                contentType: 'application/json',
                data: {sessionCode},
                success: function (budget) {
                    if (budget) {
                        app.api.proposals.buildProposalsTable(budget);
                    }
                },
                error: function(message)
                {
                    console.error(message);
                }
            });
        },
        buildProposalsTable: function (budget) {
            var proposalDescriptionElement = $("#proposal-container .description");
            proposalDescriptionElement.text('');
            proposalDescriptionElement.append($.parseHTML(budget.text));
            $("#proposal-container .budget-service-name").text('');
            $("#proposal-container .budget-amount").text('');
            $("#proposal-container .budget-max-installment").text('');
            $("#proposal-container .budget-deadline").text('');
            $("#proposal-container .budget-service-name").text(budget.service.name);
            $("#proposal-container .budget-amount").text(budget.amount);
            var pDesktopTable = $("#proposal-container .desktop .proposals-table");
            pDesktopTable.text('');
            var pMobileTable = $("#proposal-container .mobile .proposals-table");
            pDesktopTable.text('');
            pMobileTable.text('');
            if (budget.custom) {
                $('.resultOrcamento').addClass('ocultarResultadoOrcamento');
            } else {
                $('.resultOrcamento').removeClass('ocultarResultadoOrcamento');
                $("#proposal-container .budget-service-name").text(budget.service.name);
                $("#proposal-container .budget-amount").text(budget.service.metric);
                $("#proposal-container .budget-max-installment").text(budget.maxInstallment);
                $("#proposal-container .budget-deadline").text(budget.deadline);
                $.each(budget.proposals, function (i, proposal) {
                    var maisEscolhida = proposal.plan.toLocaleLowerCase() === 'express' ? 'maisEscolhida' : '';
                    // Build desktop proposals table
                    var desktopTableTemplate =
                        '<div class="col-md-3 '+ maisEscolhida +'">' +
                        '<form class="areaProposta removeBottomLeftRadius" id="'+ proposal.plan.toLowerCase() +'">' +
                        '<h3 class="'+proposal.plan.toLowerCase()+'">'+ proposal.plan.toUpperCase() +'</h3>' +
                        '<div class="titulo"><p style="border-top: none;">' + proposal.cashPirce + '</p></div>' +
                        '<div class="titulo" style="height: 79px;"><p>'+ proposal.installmentPrice +'</p></div>' +
                        '<div class="titulo"><p>'+ proposal.deadline +'</p></div>' +
                        '<div class="enviarArquivos removeBottomLeftRadius"><p><a title="Enviar arquivos" class="'+proposal.plan.toLowerCase()+'" href="https://audiotext.wetransfer.com/" target="_blank">Enviar Arquivos</a></p></div>' +
                        '<form>' +
                        '<div>';
                    pDesktopTable.append(desktopTableTemplate);
                    // Build mobile proposals table
                    var mobileTabletemplate =
                        '<div class="row" style="margin-top: 50px;">' +
                        '<div class="col-xs-6">' +
                        '<div class="menuInfo leftBorderRadius">' +
                        '<div class="tempoAudio">' +
                        '<p class="budget-service-name">'+ budget.service.name +'</p>' +
                        '<span class="budget-amount">'+ budget.service.metric +'</span>' +
                        '</div>' +
                        '<div class="titulos">' +
                        '<h3>À vista</h3>' +
                        '</div>' +
                        '<div class="titulos">' +
                        '<h3 style="height: 50px;">Parcelado</h3>' +
                        '<p class="budget-max-installment">'+ budget.maxInstallment +'</p>' +
                        '</div>' +
                        '<div class="titulos">' +
                        '<h3 style="height: 51px;">Prazo</h3>' +
                        '<p>dias úteis</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-xs-6 '+ maisEscolhida +'">' +
                        '<form class="areaProposta rightBorderRadius" id="'+ proposal.plan.toLocaleLowerCase() +'" title="Escolher proposta">' +
                        '<h3 class="'+proposal.plan.toLowerCase()+'">'+ proposal.plan.toUpperCase() +'</h3>' +
                        '<div class="titulo">' +
                        '<p style="border-top: none;">' + proposal.cashPirce + '</p>' +
                        '</div>' +
                        '<div class="titulo" style="height: 79px;">' +
                        '<p>'+ proposal.installmentPrice +'</p>' +
                        '</div>' +
                        '<div class="titulo">' +
                        '<p>'+ proposal.deadline +'</p>'+
                        '</div>' +
                        '</form>' +
                        '</div>' +
                        '<div class="col-xs-12">' +
                        '<div class="enviarArquivos instant" style="text-align: center; background-color: rgb(241,90,34); padding: 15px;border-radius: 5px; margin-top: 15px;">' +
                        '<a title="Enviar arquivos" style="color: #fff; text-decoration: none; font-weight: bold; font-size: 20px;" href="https://audiotext.wetransfer.com/" target="_blank">Enviar Arquivos</a>' +
                        '</div>' +
                        '</div>';
                    pMobileTable.append(mobileTabletemplate);
                });
            }
        }
    },
    captcha: {
        isChecked: function () {
            return grecaptcha && grecaptcha.getResponse().length !== 0;
        },
        reset: function () {
            return grecaptcha && grecaptcha.reset();
        },
        execute: function () {
            return grecaptcha && grecaptcha.execute();
        }
    },
    validator: {
        enableParticipantsAmountField: function (service, value = '') {
            var participantsAmountField = $("#budget-participantsAmount");
            var participantsAmountFieldValidation = $("#validation-participantsAmount");
            if (service === 'transcricao') {
                participantsAmountField.val(value);
                participantsAmountField.show();
            } else {
                participantsAmountField.val('1');
                participantsAmountField.hide();
                participantsAmountFieldValidation.removeClass('compoInvalido');
            }

        },
        validateForm: function (form) {
            return app.api.validator.checkFormFields(form);
        },
        checkFormFields: function (form) {
            var formId = form.attr('id');
            var requiredFields = app.api.validator.getRequiredFields(formId);
            var fields = form.serializeArray();
            var isValid = true;
            $.each(fields, function(i) {
                var key = fields[i].name;
                var value = fields[i].value;
                if (requiredFields.indexOf(key) > -1 ) {
                    if (value.trim() === '') {
                        app.api.validator.displayError(key);
                        isValid = false;
                    } else {
                        app.api.validator.removeError(key);
                    }
                }
            });
            return isValid;
        },
        getRequiredFields: function (step) {
            var requiredFields = null;
            switch (step) {
                case 'primeiraEtapa':
                    requiredFields = [
                        'username',
                        'email',
                        'phone',
                        'serviceCode',
                        'participantsAmount',
                    ];
                    break;
                case 'segundaEtapa':
                    requiredFields = [
                        'amount',
                        'finalityCode',
                        'languageCode',
                        'howDidMeetUs',
                        'g-recaptcha-response',
                    ];
                    break;
                default:
                    requiredFields = [];

            }
            return requiredFields;
        },
        displayError: function (key) {
            var field = $("#validation-"+key);
            field.addClass('compoInvalido');
            field.text('Campo de preenchimento obrigatório!');
        },
        removeError: function (key) {
            var field = $("#validation-"+key);
            field.removeClass('compoInvalido');
            field.text('');
        },
    }
};
